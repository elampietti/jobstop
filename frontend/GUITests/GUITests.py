from selenium import webdriver
import unittest
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
import time

# path for the web driver, download onto local machines for now
PATH = "C:\Program Files (x86)\chromedriver.exe"
# url we are testing
d_url = "http://localhost:3000"  # development
p_url = "https://www.jobstop.me"  # production


class JobStopGuiTests(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(PATH)
        self.driver.get(d_url)

    def test_title(self):
        self.assertEqual("JobStop", self.driver.title)

    def test_home_contents(self):
        # check for the presence of a nav bar
        try:
            elem = self.driver.find_elements_by_id("myNavBar")
        except NoSuchElementException:
            assert False
        # check for presence of 3 buttons to navigate to each of our model pages
        try:
            elems = self.driver.find_elements_by_class_name("m-3")
            if len(elems) != 3:
                assert False
        except NoSuchElementException:
            assert False
        # check for presence of footer
        try:
            elem = self.driver.find_element_by_class_name("main-footer")
        except NoSuchElementException:
            assert False

    def test_home_navigation_navbar(self):
        link_text = "Housing"
        links = WebDriverWait(self.driver, 10).until(
            EC.presence_of_all_elements_located((By.LINK_TEXT, link_text))
        )
        #first test navbar link
        links[0].click()
        new_url = d_url + "/" + link_text.lower()
        if self.driver.current_url != new_url:
            assert False
        self.driver.back()
        links = WebDriverWait(self.driver, 10).until(
            EC.presence_of_all_elements_located((By.LINK_TEXT, link_text))
        )
        #then test icon link
        links[1].click()
        new_url = d_url + "/" + link_text.lower()
        if self.driver.current_url != new_url:
            assert False
        self.driver.back()

        link_text = "Climate"
        links = WebDriverWait(self.driver, 10).until(
            EC.presence_of_all_elements_located((By.LINK_TEXT, link_text))
        )
        #first test navbar link
        links[0].click()
        new_url = d_url + "/" + link_text.lower()
        if self.driver.current_url != new_url:
            assert False
        self.driver.back()
        links = WebDriverWait(self.driver, 10).until(
            EC.presence_of_all_elements_located((By.LINK_TEXT, link_text))
        )
        #then test icon link
        links[1].click()
        new_url = d_url + "/" + link_text.lower()
        if self.driver.current_url != new_url:
            assert False
        self.driver.back()

        link_text = "Jobs"
        links = WebDriverWait(self.driver, 10).until(
            EC.presence_of_all_elements_located((By.LINK_TEXT, link_text))
        )
        #first test navbar link
        links[0].click()
        new_url = d_url + "/" + link_text.lower()
        if self.driver.current_url != new_url:
            assert False
        self.driver.back()
        links = WebDriverWait(self.driver, 10).until(
            EC.presence_of_all_elements_located((By.LINK_TEXT, link_text))
        )
        #next test icon link
        links[1].click()
        new_url = d_url + "/" + link_text.lower()
        if self.driver.current_url != new_url:
            assert False
        self.driver.back()

        link_text = "About"
        links = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, link_text))
        )

    def test_home_navigation_buttons(self):
        buttons = self.driver.find_elements_by_tag_name("Button")
        for button in buttons:
            button.click()
            if self.driver.current_url == d_url:
                assert False

    def test_housing_model_page(self):
        link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Housing"))
        )
        # in hosuing model page now
        link.click()
        # allow for hosuing data to load in
        time.sleep(5)
        # test if we are on the right page
        elem = self.driver.find_element_by_tag_name("h2")
        self.assertEqual(elem.text, "Housing Data")
        # initially, 5 city's data should be present
        elems = self.driver.find_elements_by_tag_name("tr")
        self.assertEqual(len(elems), 6)

    def test_city_details_page(self):
        link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Housing"))
        )
        # in hosuing model page now
        link.click()
        # allow for hosuing data to load in
        time.sleep(5)
        # get the first 'view details' link
        city_page_link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "View Details"))
        )
        city_page_link.click()
        # we should be in Albuquerque, New Mexico Page:
        elem = self.driver.find_element_by_tag_name("h1")
        self.assertEqual(elem.text, "ALBUQUERQUE, NEW MEXICO Housing")

    def test_job_model_page(self):
        # Navigate to the job navigation page
        link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Jobs"))
        )
        link.click()
        time.sleep(10)

        # test if we are on the right page
        elem = self.driver.find_element_by_tag_name("h1")
        self.assertEqual(elem.text, "Job Listings")

        # There should be 5 jobs loaded
        elems = self.driver.find_elements_by_tag_name("tr")
        self.assertEqual(len(elems), 6)

    def test_job_details_page(self):
        # Navigate to job first job instance page
        link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Jobs"))
        )
        link.click()
        time.sleep(10)
        city_page_link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "View Details"))
        )
        city_page_link.click()

        # Navigate to the first job (which should be a position at Intuit)
        time.sleep(10)
        elem = self.driver.find_element_by_tag_name("h2")
        self.assertEqual(elem.text, "Intuit")

    def test_job_to_housing_link(self):
        # Navigate to job first job instance page
        link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Jobs"))
        )
        link.click()
        time.sleep(10)
        city_page_link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "View Details"))
        )
        city_page_link.click()
        time.sleep(10)

        # Navigate to housing page of Alburquerque
        city_page_link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "City Housing"))
        )
        city_page_link.click()
        elem = self.driver.find_element_by_tag_name("h1")
        self.assertEqual(elem.text, "ALBUQUERQUE, NEW MEXICO Housing")

    def test_job_to_climate_link(self):
        # Navigate to job first job instance page
        link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Jobs"))
        )
        link.click()
        time.sleep(10)
        city_page_link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "View Details"))
        )
        city_page_link.click()
        time.sleep(10)

        # Navigate to climate page of Alburquerque
        city_page_link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Climate Details"))
        )
        city_page_link.click()
        elem = self.driver.find_element_by_tag_name("h2")
        self.assertEqual(
            elem.text, "A Typical Day This Month in Albuquerque, New Mexico"
        )

    def test_climate_navigation_page(self):
        # Navigate to climate navigation page
        link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Climate"))
        )
        link.click()
        # allow for climate data to load in
        time.sleep(5)
        # test if we are on the right page
        elem = self.driver.find_element_by_tag_name("h1")
        self.assertEqual(elem.text, "Summary Climate Data")
        # initially, 5 city's data should be present
        elems = self.driver.find_elements_by_tag_name("tr")
        self.assertEqual(len(elems), 6)

    def test_climate_instance_page(self):
        link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Climate"))
        )
        link.click()
        time.sleep(5)
        climate_link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "View Details"))
        )
        climate_link.click()
        elem = self.driver.find_element_by_tag_name("h1")
        self.assertEqual(elem.text, "Albuquerque, New Mexico Climate Page")

    def test_climate_to_housing_link(self):
        link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Climate"))
        )
        link.click()
        time.sleep(5)
        housing_page_link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Housing Details"))
        )
        housing_page_link.click()
        elem = self.driver.find_element_by_tag_name("h1")
        self.assertEqual(elem.text, "ALBUQUERQUE, NEW MEXICO Housing")

    def test_about_page(self):
        link_text = "About"
        link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, link_text))
        )
        link.click()
        time.sleep(2)
        elem = self.driver.find_element_by_tag_name("h1")
        self.assertEqual(elem.text, "About Us")
        elems = self.driver.find_elements_by_class_name("m-2")
        self.assertEqual(len(elems), 5)  # 5 cards
    
    def test_housing_pagination(self):
        link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Housing"))
        )
        link.click()
        try:
            elem = self.driver.find_elements_by_class_name("MuiTablePagination-\
                select")
        except NoSuchElementException:
            assert False

    def test_climate_pagination(self):
        link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Climate"))
        )
        link.click()
        try:
            elem = self.driver.find_elements_by_class_name("MuiTablePagination-\
                select")
        except NoSuchElementException:
            assert False
    
    def test_jobs_pagination(self):
        link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Jobs"))
        )
        link.click()
        try:
            elem = self.driver.find_elements_by_class_name("MuiTablePagination-\
                select")
        except NoSuchElementException:
            assert False
    
    def test_housing_search(self):
        link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Housing"))
        )
        link.click()
        elem = None
        try:
            elem = self.driver.find_elements_by_class_name("MuiInputAdornment-\
                root")
        except NoSuchElementException:
            assert False
    
    def test_jobs_search(self):
        link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Jobs"))
        )
        link.click()
        elem = None
        try:
            elem = self.driver.find_elements_by_class_name("MuiInputAdornment-\
                root")
        except NoSuchElementException:
            assert False
    
    def test_climate_search(self):
        link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Climate"))
        )
        link.click()
        elem = None
        try:
            elem = self.driver.find_elements_by_class_name("MuiInputAdornment-\
                root")
        except NoSuchElementException:
            assert False
    
    def test_housing_filter(self):
        link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Housing"))
        )
        link.click()
        elem = None
        try:
            elem = self.driver.find_elements_by_class_name("MuiButton-label")
        except NoSuchElementException:
            assert False
    
    def test_climate_filter(self):
        link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Climate"))
        )
        link.click()
        elem = None
        try:
            elem = self.driver.find_elements_by_class_name("MuiButton-label")
        except NoSuchElementException:
            assert False
    
    def test_jobs_filter(self):
        link = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.LINK_TEXT, "Jobs"))
        )
        link.click()
        elem = None
        try:
            elem = self.driver.find_elements_by_class_name("MuiButton-label")
        except NoSuchElementException:
            assert False

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
