import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import { expect } from 'chai';

/* things to test app module */
import App from './App';

/* things to test Navigation.tsx */
import Navigation from './components/Navigation';
import { Navbar, Nav} from 'react-bootstrap';
import { Route, Link } from 'react-router-dom';
import Select from 'react-select';

/* things to test Footer.tsx */
import Footer from './components/Footer';

/* things to test Home.tsx */
import Home from './components/Home';
import MyCard from './components/HomeCards/MyCards';

/* things to test HousingNav.tsx */
import {TableCell, DialogTitle, InputLabel} from '@material-ui/core';

/* things to test JobNav.tsx */
import JobGridElement from './components/JobGridElement';

import ClimateGridElement from './components/ClimateGridElement'

import Adapter from 'enzyme-adapter-react-16';
import HousingGridElement from './components/HousingGridElement';
import FilterDialog from './components/FilterDialog';
import CustomSlider from './components/CustomSlider';
configure({ adapter: new Adapter() });

describe('App component testing', function() {
  it('contains Navigation', function(){
    const wrapper = shallow(<App />);
    expect(wrapper.find(Navigation).length).to.eq(1);
  });
  it('contains footer', function() {
    const wrapper = shallow(<App />); 
    expect(wrapper.find(Footer).length).to.eq(1);
  });
  it('app is wrapped in a page container', function(){
    const wrapper = shallow(<App />);
    expect(wrapper.find('div.page-container').length).to.eq(1);
  });
});

describe('Navigation component testing', function(){
  it('displays properly styled nav bar', function(){
    const wrapper = shallow(<Navigation />);
    expect(wrapper.find(Navbar).first().props().bg).to.equal('dark');
    expect(wrapper.find(Navbar).first().props().variant).to.equal('dark');
  });
  it('contains a select element for site wide search', function(){
    const wrapper = shallow(<Navigation />);
    expect(wrapper.find(Select).length).to.eq(1);
  });
  it('contains 6 links to pages', function(){
    const wrapper = shallow(<Navigation/>);
    expect(wrapper.find(Nav.Link).length).to.equal(6);
  });
});

describe('Footer component testing', function(){
  it("is styled using main-footer", function(){
    const wrapper = shallow(<Footer />);
    expect(wrapper.find('div.main-footer').length).to.eq(1);
  });
  it("contains 5 links", function(){
    const wrapper = shallow(<Footer/>);
    expect(wrapper.find('a').length).to.eq(5);
  });
})

describe('Home component testing', function(){
  it("it contains the correct information", function(){
    const wrapper = shallow(<Home/>);
    expect(wrapper.find('h4').first().text()).to.eq(
      'We\'re here to help you find the best job for your needs');
  });
  it("it contains three buttons to navigate to each of our models", function(){
    const wrapper = shallow(<Home/>);
    expect(wrapper.find(MyCard).length).to.eq(1);
  })
});



describe("Housing Grid Element Testing", function(){
  it('Housing Grid element has 9 Table cells', function(){
    const wrapper = shallow(<HousingGridElement cityName='Austin' 
    MLPAH={0} MLPFAH={0} MRPAH={0} MRPFAH={0} PHDVAH={0} PHIVAH={0} id={1}/>);
    expect(wrapper.find(TableCell).length).to.eq(9);
  });
  it('Housing Grid Element has link to dynamic city instance page', function(){
    const wrapper = shallow(<HousingGridElement cityName='Austin'
                             MLPAH={0} MLPFAH={0} MRPAH={0} MRPFAH={0}
                            PHDVAH={0} PHIVAH={0} id={1}/>);
    expect(wrapper.find(Link).first().props().to).to.eql(
      '/housing/city_details/1')
  });
  it('Housing Grid Element has link to dynamic climate instance page', 
  function(){
    const wrapper = shallow(<HousingGridElement cityName='Austin'
                             MLPAH={0} MLPFAH={0} MRPAH={0} MRPFAH={0}
                             PHDVAH={0} PHIVAH={0} id={1}/>);
    expect(wrapper.find(Link).last().props().to).to.eql(
      '/climate/city_climate/1')                         
  });
});


describe('FilterDialog tests', function(){
  it('contains correct title', function(){
    const wrapper = mount(<FilterDialog 
      ranges={{1:[1, 100], 2:[2, 50]}}
      columns={[1, 2]}
      popupOpen={true}
      onClose={() => {}}
    />)
    expect(wrapper.find(DialogTitle).first().text()).to.eq('Add Filter')
  }) 
  it('contains slider', function(){
    const wrapper = mount(<FilterDialog 
      ranges={{1:[1, 100], 2:[2, 50]}}
      columns={[1, 2]}
      popupOpen={true}
      onClose={() => {}}
    />)
    expect(wrapper.find(CustomSlider).length).to.eq(1); 
  })
  it('contains a selection element', function(){
    const wrapper = mount(<FilterDialog 
      ranges={{1:[1, 100], 2:[2, 50]}}
      columns={[1, 2]}
      popupOpen={true}
      onClose={() => {}}
    />)
    expect(wrapper.find(InputLabel).first().text()).to.eq('Select Column')
  })
})

describe('Jobs Navigation components testing', function(){
  it('Job Grid element has 7 Table cells', function(){
    const wrapper = shallow(<JobGridElement jobID={1} company={""}  title={""} 
      category={""} location={"Austin"} type={""}/>);
    expect(wrapper.find(TableCell).length).to.eq(7);
  });
  it('Job Grid Element has link to dynamic job instance page', function(){
    const wrapper = shallow(<JobGridElement jobID={1} company={""}  title={""} 
      category={""} location={""} type={""}/>);
    expect(wrapper.find(Link).first().props().to).to.eql("/jobs/job_details/1")
  });
});

describe('Climate Details Navigation Table testing', function() {
  it('ClimateGridElement has 7 Table cells', function(){
    const wrapper = shallow(<ClimateGridElement cityName={''} lowTemp={0} 
      highTemp={0} precipitation={0} typicalHumidity={0} typicalWindSpeed={0} 
      cityId={500}/>);
    expect(wrapper.find(TableCell).length).to.eq(8);
  });
  it('ClimateGridElement has link to dynamic job instance page', function(){
    const wrapper = shallow(<ClimateGridElement cityName={'Austin'} lowTemp={0} 
      highTemp={0} precipitation={0} typicalHumidity={0} typicalWindSpeed={0} 
      cityId={500}/>);
    expect(wrapper.find(Link).first().props().to).to.eql(
      '/climate/city_climate/500')
  });
});
