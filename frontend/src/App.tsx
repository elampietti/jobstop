import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import './App.css';
import Footer from './components/Footer'

import Navigation from './components/Navigation';

function App() {
  return (  
    <div className="page-container">
    <div className="content-wrap">
      <BrowserRouter>
        <div>
          <Navigation />
        </div> 
     </BrowserRouter>
    </div> 
    <Footer></Footer>
    </div>   
 );
}

export default App;
