import React from 'react';

export default function LoaderIcon(){
    return (
        <div style={{display: 'flex',  justifyContent:'center', 
            alignItems:'center', height: '100vh'}}>
            <i 
               className="fa fa-refresh fa-spin fa-5x"
            />
         </div>
    )
}