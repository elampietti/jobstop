import React from 'react';
import './Climate.css';
import Chart from 'react-google-charts';

export default function MonthClimate(props: any) {

    const combinedTemps = props['combinedTemps'];
    const formattedPrecipitation = props['formattedPrecipitation'];
    
    return (
        <div className="month">
                <h2>Weather by Month</h2>
                <div className="d-flex">
                <div className="two-box">
                <h4>Average Temperature by Month</h4>
                <Chart
                    height={'300px'}
                    chartType='LineChart'
                    loader={<div>Loading Temperature...</div>}
                    data={combinedTemps}
                    options={{
                        hAxis: {
                          title: 'Month'
                        },
                        vAxis: {
                          title: 'Temperature (°C)'
                        },
                      }}
                />
                </div>
                <div className="two-box">
                <h4>Average Precipitation by Month</h4>
                <Chart
                    height={'300px'}
                    chartType='Bar'
                    loader={<div>Loading Precipitation...</div>}
                    data={formattedPrecipitation}
                    options={{
                        hAxis: {
                          title: 'Month'
                        },
                        vAxis: {
                          title: 'Precipitation (mm)'
                        },
                      }}
                />
                </div>
            </div>
        </div>
    )

}