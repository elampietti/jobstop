import React, { useEffect, useState } from 'react';
import './Climate.css';
import { Button } from '@material-ui/core';
import housing from './TransitionImages/housing.svg'
import DayClimate from './DayClimate';
import MonthClimate from './MonthClimate';
import useAxios from 'axios-hooks';
import LoaderIcon from './LoadingIcon';
import Error from './Error';
import JobGrid from './JobGrid';

function CityClimate(props: any) {

    const cityId = props.match.params.city_idx;
    
    const [jobRecords, setJobRecords] = useState([] as any);

    /* api request setup */
    const [{ data:result1, loading:loading1, error:error1 }] =
    useAxios(
        {
            url: 'https://api.jobstop.me/climate?by=cityIdx&value='+cityId
        }
    )
    
    const [{ data:result2, loading:loading2, error:error2 }] =
    useAxios(
        {
            url: 'https://api.jobstop.me/cities/cityData?cityIdx='+cityId
        }
    )

    /* empty filters to display job grid */
    const searchFn = {fn:(items: any) => {return items}, str:''};
    const filterFn = { filters: []};

    const [city, setCity] = useState('');

    useEffect(() => {

        if(!loading1 && !loading2){

            /* job related */
            let jobsData = result2['jobs']
            let myRows = [];
            let cityNameSet = false;
            for(var key in jobsData){
                let jobData = jobsData[key];
                if (!cityNameSet){
                    setCity(jobData['location']);
                    cityNameSet = true;
                }
                let jobRecord = { 
                    jobID : Number(key),
                    ...jobData
                }
                myRows.push(
                    jobRecord
                )
            }
            setJobRecords(myRows)
        }
    }, [loading1, loading2, result1, result2])

    if (loading1 || loading2){
        return (
            <LoaderIcon/>
        )
    }

    if(error1 || error2){
        return(
            <Error/>
        )
    }

    return (
        <div className="climate">
            <div className="row">
                <div className="col">
                    <h1>{city} Climate Page</h1>
                </div>
                <div className="col">
                <h3>Housing Data:</h3>
                    <Button
                    variant="outlined"
                    href={'/housing/city_details/'+cityId}
                    >
                        <img src={housing} className="housing-photo" alt=''/>
                    </Button>
                </div>
            </div>
            
            <DayClimate {...result1[Object.keys(result1)[0]]}/>
            <MonthClimate {...result1[Object.keys(result1)[0]]}/>

            <div className='table-box'>
                <h3>Jobs in the area:</h3>
                <div className="model-table">
                    <JobGrid
                    records={jobRecords}
                    searchFn={searchFn}
                    filterFn={filterFn}
                    />
                </div>
            </div>
        </div>
    );
}

export default CityClimate;