import React from 'react';

export default function DayClimate(props: any) {

    let currentDate = new Date();
    let currentMonth = currentDate.getMonth() + 1;

    const summary = props['summary'];
    const lowTemps = props['lowTemps'];
    const highTemps = props['highTemps']; 
    const precipitation = props['precipitation'];
    const typicalHumidity = props['humidities'][currentMonth];
    const typicalWindSpeed = props['windSpeeds'][currentMonth];
    
    return (
        <div className="day">
                <h2>A Typical Day This Month</h2>
                <div className="d-flex">
                    <div className="two-box">
                        <div style={{fontSize: "20px"}}>
                            <div>Overall Description: {
                                (String(summary).charAt(0).toUpperCase()
                                 + summary.slice(1))
                            }
                            </div>
                            <img style={{height: "128px", width: "128px"}}
                                src=
                                {
                                ('https://ssl.gstatic.com/onebox/weather/64/' + 
                                summary + '.png')
                                } alt="">
                            </img>
                        </div>
                        
                    </div>
                    <div className="two-box" style={{fontSize: "20px"}}>
                        <div>Today's High: {highTemps[currentMonth]}°C</div>
                        <div>Today's Low: {lowTemps[currentMonth]}°C</div>
                        <div>Average Precipitation Per Day: {
                            (precipitation[currentMonth] / 30).toFixed(1)
                            } millimeters</div>
                        <div>
                            Average Humidity Today: {
                                Number(typicalHumidity).toFixed(1)
                            }%
                            </div>
                        <div>
                            Average Wind Speed: {
                                Number(typicalWindSpeed).toFixed(1)
                            } km/h
                        </div>
                    </div>
                </div>
            </div>
    )

}