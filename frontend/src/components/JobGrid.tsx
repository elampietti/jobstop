import React from 'react';
import JobGridElement from './JobGridElement';
import { ModelGridProps } from './ModelGridProps';
import { ModelTable } from './ModelTable';

export default function JobGrid(props: ModelGridProps) {
   
    /* Column information */
    const headCells = [
       { id: 'id', label: 'ID' },
       { id: 'company', label: 'Company', canSearch: true },
       { id: 'title', label: 'Job Title', canSearch: true },
       { id: 'category', label: 'Job Category', canSearch: true },
       { id: 'location', label: 'Location', canSearch: true },
       { id: 'type', label: 'Employment Type', canSearch: true },
       { id: 'moreDetails', label: 'more details', disableSorting: true },
    ]
 
    /* function converts raw record information (JSON) to react component */
    function recordToElem(records:any){
       return (
          records.map((record:any, idx:number) => {
             return(
                <JobGridElement {...record} key={idx}/>
             )
          })
       )
    }
 
    return (
       <ModelTable rawRecords={props.records} dispRecords={recordToElem}
         headCells={headCells} searchFn={props.searchFn} 
            filterFn={props.filterFn}/>
    )
 }