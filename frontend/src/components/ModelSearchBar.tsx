import {InputAdornment, Toolbar } from '@material-ui/core';
import Input from './Input';
import { Search } from '@material-ui/icons';
import React from 'react';

type SearchBarProps = {
    label: string,
    changeFn: Function
}

export default function SearchBar(props: SearchBarProps){
    return (
        <Toolbar>
            <Input 
                label = {props.label}
                InputProps = {{
                startAdornment:(
                <InputAdornment position="start">
                    <Search/> 
                </InputAdornment>
                )
                }}
                onChange = {props.changeFn}
            />
        </Toolbar>
    )
}