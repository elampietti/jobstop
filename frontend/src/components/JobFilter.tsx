import React from 'react';
import { createStyles, makeStyles, 
    useTheme, Theme } from '@material-ui/core/styles';
import { InputLabel, MenuItem, FormControl, Select } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }),
);


function getStyles(name: string, personName: string, theme: Theme) {
    return {
        fontWeight:
            personName.indexOf(name) === -1
                ? theme.typography.fontWeightRegular
                : theme.typography.fontWeightMedium,
    };
}

export default function JobSelect(props: any) {
    let names: Array<string> = props.ar;
    let filter: Function = props.filterFunc;
    let col: string = props.col;
    const classes = useStyles();
    const theme = useTheme();
    const [personName, setPersonName] = React.useState('');


    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setPersonName(event.target.value as string);
        filter(col, event.target.value as string);
    };

    return (
        <div>
            <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">{props.name}
                </InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={personName}
                    onChange={handleChange}
                >
                    {names.map((name) => (
                        <MenuItem key={name} value={name} 
                            style={getStyles(name, personName, theme)}>
                            {name}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        </div>
    );
}
