import { TableRow, TableCell } from "@material-ui/core";
import React from "react";
import { Link } from "react-router-dom";

export type JobGridElementProps = {
    jobID: number,
    company: string,
    title: string,
    location: string,
    category: string,
    type: string,
 }
 
 export default function JobGridElement(props: JobGridElementProps) {
    const id = props.jobID;
    const company = props.company;
    const title = props.title;
    const location = props.location;
    const category = props.category;
    const type = props.type;
 
    const createMarkup = (html: string) => {
       return { __html: html }
    }


   let cells = [company, title, category, location, type];
    return (
       <TableRow>
          <TableCell>{id}</TableCell>
          {
             cells.map((data:any, index:number) => {
                return (
                   <TableCell dangerouslySetInnerHTML={createMarkup(data)}
                      key={index}/>
                )
             })
          }
          <TableCell>
             <u><Link to={`/jobs/job_details/${id}`}>View Details</Link></u>
          </TableCell>
       </TableRow>
    )
 }