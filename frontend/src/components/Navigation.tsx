import React, { useEffect, useState, } from 'react';
import { Navbar, Nav, Button} from 'react-bootstrap';
import { NavLink, useHistory } from 'react-router-dom';
import AppRoutes from './AppRoutes';
import 'bootstrap/dist/css/bootstrap.css';
import Select from 'react-select';
import jobstopImage from '../images/jobstop_white.png';
import useAxios from 'axios-hooks';

function Navigation (){

    const [options, setOptions] = useState([] as any);
    let search = "";
    
    const [{ data:data1, loading:loading1 }] = useAxios(
        {
            url: 'https://api.jobstop.me/cities/summary'
        }
    )

    const [{ data:data2, loading:loading2 }] = useAxios(
        {
            url: "https://api.jobstop.me/jobs?by=summary"
        }
    )

    useEffect(() => {

        if(!loading1 && !loading2){
            let tempOptions = [] as any;
            // cities
            for (var key1 in data1) {
                tempOptions.push(key1);
            }
            // jobs
            for (var key2 in data2) {
                tempOptions.push(data2[key2]['company']);
                tempOptions.push(data2[key2]['category']);    
            }
            // remove duplicates
            let newOptions = [] as any
            let uniqueOptions = [...new Set(tempOptions)]
            for (var elem in uniqueOptions) {
                newOptions.push({value: uniqueOptions[elem], 
                                label: uniqueOptions[elem]});
            }
            setOptions(newOptions)
        }
    }, [loading1, loading2, data1, data2])

    let history = useHistory();

    const handleInputChange = (inputValue : any) => {
        if (inputValue) {
            search = inputValue;
        }
    }

    const updateSearch = (query: any, { action } : {action : any}) => {
        if (action === 'select-option') {
            search = query.value;
        }
    };

    const buttonSearch = () => {
        history.push(`/search_results/${search}`)
    }

    const handleKeyDown = (event : any) => {
        if (event.key === 'Enter') {
            history.push(`/search_results/${search}`)
        }
    };

    return (
        <div>
            <div id='myNavBar'>
                <Navbar bg="dark" variant="dark">
                    <Navbar.Brand href="/">
                        <img
                            alt=""
                            src={jobstopImage}
                            width="101"
                            height="30"
                            className="d-inline-block align-top"
                        />
                    </Navbar.Brand>
                    <Navbar.Collapse>
                        <Nav className="mr-auto">
                            <Nav.Link href="/housing">Housing</Nav.Link>
                            <Nav.Link href="/climate">Climate</Nav.Link>
                            <Nav.Link href="/jobs">Jobs</Nav.Link>
                            <Nav.Link href="/about">About</Nav.Link>
                            <Nav.Link href="/visual">Visualizations</Nav.Link>
                            <Nav.Link 
                                href="/providervisual">ProviderVisualizations
                            </Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                    <Select className="col-md-2" placeholder='Type to search' 
                        options={options} onKeyDown={handleKeyDown} 
                        onInputChange={handleInputChange} 
                        onChange={updateSearch}/>
                    <Button variant="outline-info" onClick={buttonSearch}>
                        <NavLink style={{color:'white', textDecoration: 'none'}}
                            to={`/search_results/${search}`}
                            >Search</NavLink>
                    </Button>
                </Navbar>
            </div>
            <AppRoutes/>
        </div>
    );
}
 
export default Navigation;