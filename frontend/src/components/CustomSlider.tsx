import { Slider, withStyles } from '@material-ui/core';

/* styles for slider */
export const CustomSlider = withStyles({
    valueLabel: {
        left: 'calc(-50% + 12px)',
        top: -22,
        '& *': {
          background: 'transparent',
          color: '#000',
        },
      },
})(Slider);

export default CustomSlider;