import React from 'react';
import './Model.css';
import { useState, useEffect } from 'react';
import { Button, Typography } from '@material-ui/core';
import JobSelect from './JobFilter';
import ModelSearchBar from './ModelSearchBar';
import JobGrid from './JobGrid';
import useAxios from 'axios-hooks';
import LoaderIcon from './LoadingIcon';
import Error from './Error';

function JobNav() {

   /* api request setup */
   const [{ data, loading, error }] = useAxios(
      {
         url: 'https://api.jobstop.me/jobs?by=summary'
      }
   )

   /* Dynamic variables for sorting and filtering */
   const [allRecords, setAllRecords] = useState([] as any);
   const [searchFn, setSearchFn] = useState({
      fn: (items: any) => { return items }, str: ''
   });
   const [numFilters, setNumFilters] = useState({ filters: [] as any })
   const [filterMap, setFilterMap] = useState({} as any);

   /* Display instances on search */
   const handleSearch = (e: any) => {
      let target = e.target;
      setSearchFn({
         fn: (items: any) => {
            if (target.value === "")
               return items;
            else
               return items.filter((x: any) =>
                  x['location'].includes(target.value) ||
                  x['company'].includes(target.value) ||
                  x['title'].includes(target.value) ||
                  x['category'].includes(target.value) ||
                  x['type'].includes(target.value)
               )
         },
         str: target.value
      })
   }

   /* Display only instances with filtered columns */
   function handleFilter(col: string, value: string) {
      if (value != null && value !== "") {
         let newFilter: Function = (items: any) => {
            return items.filter((x: any) => x[col].includes(value));
         }
         let currentFilters: any = []
         let fm = filterMap;
         fm[col] = newFilter;

         // We store the filters in a dict so the same column filters override
         for (const [key, value] of Object.entries(fm)) {
            if (key != null && value != null) {
               currentFilters.push(value);
            }
         }

         setNumFilters({ filters: currentFilters });
         setFilterMap(fm);
      }
   };

   /* Remove all current filters */
   const handleClear = () => {
      setFilterMap({});
      setNumFilters({ filters: [] });
   }

   /* Populate table using API */
   useEffect(() => {

      if (!loading) {
         let myRecords = [];
         for (var key in data) {
            let jobData = data[key];
            myRecords.push(
               {
                  jobID: Number(key),
                  ...jobData
               }
            )
         }
         setAllRecords(myRecords)
      }
   }, [data, loading])

   /* Populate the filter dropdown */
   let companyList: Array<string> = []
   let catList: Array<string> = []
   let locList: Array<string> = []
   let typeList: Array<string> = []

   allRecords.forEach((element: any) => {
      if (companyList.indexOf(element['company']) === -1) {
         companyList.push(element['company'])
      }
      if (catList.indexOf(element['category']) === -1) {
         catList.push(element['category'])
      }
      if (locList.indexOf(element['location']) === -1) {
         locList.push(element['location'])
      }
      if (typeList.indexOf(element['type']) === -1) {
         typeList.push(element['type'])
      }
   });

   /* display spinner while loading */
   if (loading) {
      return (
         <LoaderIcon />
      )
   }

   /* error message is displayed if fetching doesn't work */
   if (error) {
      return (
         <div>
            <Error/>
         </div>
      )
   }

   let selectName = ["Company", "Job Category", "Location", "Job Type"];
   let arVal = [companyList.sort(), catList.sort(), locList.sort(),
                typeList.sort()];
   let colVal = ["company", "category", "location", "type"];

   return (
      <div className="model">
         <Typography variant="h2" align="center">Job Listings</Typography>
         <div className="model-content">
            <div className="row">
               <div>
                  <ModelSearchBar label={"search jobs"} 
                     changeFn={handleSearch}/>
               </div>
               {
                  selectName.map((text: string, idx:number) => {
                     return (
                        <JobSelect
                           key={text}
                           name={text}
                           ar={arVal[idx]}
                           filterFunc={handleFilter}
                           col={colVal[idx]}
                        />
                     )
                  })
               }
               <Button variant="outlined" onClick={handleClear}>
                  Clear Filters</Button>
            </div>
            <div className="model-table">
               <JobGrid
                  records={allRecords}
                  filterFn={numFilters}
                  searchFn={searchFn}
               />
            </div>
         </div>
      </div>
   );
}

export default JobNav;