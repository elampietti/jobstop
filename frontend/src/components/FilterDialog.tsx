import { Button, createStyles, Dialog, DialogContent,
         DialogTitle, FormControl, InputLabel, makeStyles,
         MenuItem, Select, Theme } from "@material-ui/core";
import React, { useState } from "react";
import CustomSlider from './CustomSlider';

/* styles for other components */
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    form: {
      display: 'flex',
      flexDirection: 'column',
      margin: 'auto',
      width: 'fit-content',
    },
    formControl: {
      marginTop: theme.spacing(2),
      minWidth: 120,
    },
    formControlLabel: {
      marginTop: theme.spacing(1),
    },
  }),
);

export default function FilterDialog(props:any){
    const classes = useStyles();
    /* obtained from props -> supplied by other component*/
    const { onClose, columns, ranges , popupOpen } = props;
    /* state variables */
    const [col, setCol] = useState("");
    const [open, setOpen] = useState(false);
    const [range, setRange] = useState([0, 100]);
    const [selectedRange, setSelectedRange] = useState([20, 50]);
 
    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
       /* when a column is selected: */
       setCol(event.target.value as string);
       if (event.target.value !== ''){
         setRange(ranges[event.target.value as string]);
         setSelectedRange(ranges[event.target.value as string]);
       }
    };
 
    const handleRangeChange = (event: any, newValue: number | number[]) => {
       /* when user adjusts slider values */
       setSelectedRange(newValue as number[]);
    };
 
    /* open and close of selection box */
    const handleClose = () => {
       setOpen(false);
    };
   
    const handleOpen = () => {
       setOpen(true);
    };
 
    /* functions to handle dialog close and dialog close caused due to adding */
    
    const handleDialogClose = () => {
       onClose();
    }
 
    const handleDialogAddClicked = (col: string, range: Array<number>) => {
       onClose(col, range);
    }
 
    return (
       <Dialog onClose={handleDialogClose} aria-labelledby="simple-dialog-title"
        open={popupOpen} fullWidth={true} maxWidth="sm">
         <DialogTitle id="simple-dialog-title">Add Filter</DialogTitle>
            <DialogContent>
               <form className={classes.form} noValidate>
                  <FormControl className={classes.formControl}> 
                     <InputLabel>Select Column</InputLabel>
                        <Select
                        open={open}
                        onClose={handleClose}
                        onOpen={handleOpen}
                        value={col}
                        onChange={handleChange}
                        >
                        <MenuItem value="">
                           <em>None</em>
                        </MenuItem>
                           {columns.map((column: string) => {
                              return (
                                    <MenuItem key={column} value={column}>
                                       {column} 
                                    </MenuItem>
                                 );
                              }
                           )}
                        </Select>
                  </FormControl>
               </form>
            <div>
               <p>Adjust the Range</p>
               <CustomSlider
               value={selectedRange}
               min={range[0]}
               max={range[1]}
               onChange={handleRangeChange}
               valueLabelDisplay="auto"
               aria-labelledby="range-slider"
               />
            </div>
         </DialogContent>
         <Button variant="outlined"
         onClick={() => handleDialogAddClicked(col, selectedRange)}>Add</Button>
       </Dialog>
    )
 }