import React from 'react';
import {useState, useEffect} from 'react';
import HousingGrid from './HousingGrid';
import JobGrid from './JobGrid';
import ClimateGrid from './ClimateGrid';
import useAxios from 'axios-hooks';
import LoaderIcon from './LoadingIcon';
import Error from './Error';

function SearchResults(props: any){

    /* record data for tables */
    const [housingRecords, setHousingRecords] = useState([] as any);
    const [jobRecords, setJobRecords] = useState([] as any);
    const [climateRecords, setClimateRecords] = useState([] as any);

    /* search params */
    const search = props.match.params.search
    const searchFnHC = {
        fn: (items: any) => {
            if (search === "")
                return items;
            else
                // cityName contains both the city name and state name
                return items.filter(
                    (x: any) => 
                    x['cityName'].toLowerCase().includes(search.toLowerCase())
                )
        },
        str: search
    }

    const searchFnJobs = {
        fn: (items: any) => {
           if (search === "")
              return items;
           else
              return items.filter((x: any) =>
                 x['location'].toLowerCase().includes(search.toLowerCase()) ||
                 x['company'].toLowerCase().includes(search.toLowerCase()) ||
                 x['title'].toLowerCase().includes(search.toLowerCase()) ||
                 x['category'].toLowerCase().includes(search.toLowerCase()) ||
                 x['type'].toLowerCase().includes(search.toLowerCase())
              )
        },
        str: search
     }

    const filterFn =  { filters: []};

    /* api request setup */
    const [{ data:data1, loading:loading1, error:error1 }] = 
    useAxios(
        {
            url: 'https://api.jobstop.me/cities/summary'
        }
    )

    const [{ data:data2, loading:loading2, error:error2 }] =
    useAxios(
        {
            url: 'https://api.jobstop.me/jobs?by=summary'
        }
    )

    const [{ data:data3, loading:loading3, error:error3 }] =
    useAxios(
        {
            url: 'https://api.jobstop.me/climate?by=summary'
        }
    )

    useEffect(()=>{

        if(!loading1 && !loading2 && !loading3){
            /* housing data */
            let tempHousingRecords = [];
            for (var key1 in data1) {
                let cityData = data1[key1];
                tempHousingRecords.push(
                    {
                        cityName: String(key1),
                        ...cityData
                    }
                )
            }
            setHousingRecords(tempHousingRecords)

            /* climate data */
             // initialize information needed to organize data
             let tempClimateRecords = [];
             let currentDate = new Date();
             let currentMonth = currentDate.getMonth() + 1;
 
             // push each row into a list
             for (var key3 in data3) {
                 let climateData = data3[key3];
                 let lTemp = Math.min(...climateData['lowTemps']);
                 let hTemp = Math.max(...climateData['highTemps']);
                 let prec = climateData['precipitation'].reduce
                 (
                     function (a:number, b: number) { return a + b; }
                 );
                 tempClimateRecords.push(
                    {
                        cityName:String(key3),
                        lowTemp:lTemp,
                        highTemp:hTemp,
                        precipitation: prec,
                        typicalHumidity:
                            climateData['humidities'][currentMonth],
                        typicalWindSpeed:Number
                        (
                            climateData['windSpeeds'][currentMonth]
                        ),
                        cityId: Number(climateData['cityId'])
                    }
                 )
             }
 
            setClimateRecords(tempClimateRecords)

             /* Jobs Data */
            let tempJobRecords = [];
            for (var key2 in data2) {
                let jobData = data2[key2];
                tempJobRecords.push(
                    {
                        jobID: Number(key2),
                        ...jobData
                    }
                )
            }
            setJobRecords(tempJobRecords)
        }

    }, [loading1, loading2, loading3, data1, data2, data3])

    if(loading1 || loading2 || loading3){
        return (
            <LoaderIcon/>
        )
    }
    if (error1 || error2 || error3) {
        return (
            <Error/>
        )
    }

    return (
       <div>
          <h1 style={{textAlign: "center"}}>Search Results for "{search}"</h1>
          <div>
            
            <div className="model-content">
                <h2>Housing Results</h2>
                <div className="model-table">
                    <HousingGrid
                    records={housingRecords}
                    searchFn={searchFnHC}
                    filterFn={filterFn}/>
                </div>
            </div>
            <div className="model-content">
                <h2>Job Results</h2>
                <div className="model-table">
                    <JobGrid
                    records={jobRecords}
                    searchFn={searchFnJobs}
                    filterFn={filterFn}/>
                </div>
            </div>
            <div className="model-content">
                <h2>Climate Results</h2>
                <div className="model-table">
                    <ClimateGrid
                    records={climateRecords}
                    searchFn={searchFnHC}
                    filterFn={filterFn}/>
                </div>
            </div>
          </div>
       </div>
    );
}
 
export default SearchResults;