import React, { useState, useEffect } from 'react';
import {Card, ListGroup, CardDeck, CardColumns, Button} from 'react-bootstrap'
import useAxios from 'axios-hooks';
import LoaderIcon from './LoadingIcon';
import Error from './Error';
import team from './AboutData/team_data';
import tools from './AboutData/tools_data';

function About() {

   const [commits, setCommits] = useState([] as any);
   const [issues, setIssues] = useState([] as any);
   //const unitTests = []

   /* api request setup */
   const [{ data:data1, loading:loading1, error:error1 }] =
   useAxios(
      {
          url: `https://gitlab.com/api/v4/projects/21322574/repository/commits?
          all=yes&per_page=1000&pages=1000`
      }
   )

   /* api request setup */
   const [{ data:data2, loading:loading2, error:error2 }] =
   useAxios(
      {
          url: `https://gitlab.com/api/v4/projects/21322574/issues?
          all=yes&per_page=1000&pages=1000`
      }
   )

   // Get gitlab commits/issues dynamically
   useEffect(() => {
      if(!loading1 && !loading2){
         setCommits(data1);
         setIssues(data2);
      }
   }, [loading1, loading2, data1, data2]);

   /* display spinner while loading */
   if (loading1 || loading2){
      return (
          <LoaderIcon/>
      )
   }  

  /* error message is displayed if fetching doesn't work */
   if (error1 || error2){
      return (  
         <Error/>
      ) 
   }

   return (
      <div>
         <div>
            <h1 style={{textAlign: "center"}}>About Us</h1>
            <div>
               <CardDeck>
                  <Card border="dark" className="text-center ml-5 mt-3 mb-3" 
                     style={{ width: '30rem'}}>
                  <Card.Header><Card.Title>Description</Card.Title></Card.Header>
                  <Card.Body>
                     <Card.Text>
                        This site is meant for someone who is unemployed or just
                        looking for a new job opportunity. Our intention is to 
                        allow our user to find the job that is best suited for 
                        them while minizing the overwhelming nature of the job 
                        hunt. All you have to do is provide input in at least 
                        one of these areas and we'll handle the rest.
                     </Card.Text>
                  </Card.Body>
                  </Card>
                  <Card border="dark" className="text-center mr-5 mt-3 mb-3" 
                     style={{ width: '30rem'}}>
                  <Card.Header><Card.Title>Data Insights</Card.Title></Card.Header>
                  <Card.Body>
                     <Card.Text>
                        We use data from three different models (Jobs, Housing, 
                        Climate) to find the user jobs located in a city that 
                        fits both their housing and climate preferences. These 3
                        data models are separate, yet all have a common 
                        connection through location. This connection allows us 
                        to utilize the user's preferences in these 3 areas in 
                        order to extract data for the best job and city that 
                        fits the user's budget, job, and climate preferences.
                     </Card.Text>
                  </Card.Body>
                  </Card>
               </CardDeck>
            </div>
            <h1 style={{textAlign: "center"}}>Our Team</h1>
         </div>
         <div className='text-center'>
            {team.map((member) => {
               return (
                  <Card
                  bg={'dark'}
                  text={'white'}
                  style={{ width: '16rem', display: "inline-flex"}}
                  className="m-2 p-2"
                  key={member.name}
                  >
                     <Card.Img variant="top" src={member.photo} />
                     <Card.Body>
                        <Card.Title>
                           <a target="_blank" href={member.linkedin} 
                              style={{color: "#ffffff"}} 
                              rel="noopener noreferrer"><u>{member.name}</u></a>
                        </Card.Title>
                        <Card.Subtitle className="mb-3" style={{fontSize:15}}>
                           {member.role}</Card.Subtitle>
                        <Card.Subtitle className="mb-3" style={{fontSize:15}}>
                           {member.leader}</Card.Subtitle>
                        <Card.Text>
                           {member.bio}
                        </Card.Text>
                     </Card.Body>
                     <ListGroup variant="flush">
                     <ListGroup.Item variant="dark">
                        Commits: {commits.filter((commit: any) => 
                        commit.committer_name === member.committer_name).length}
                     </ListGroup.Item>
                     <ListGroup.Item variant="dark">
                        Issues: {issues.filter((issue: any) => 
                        issue.assignees.find((assignee : any) => 
                        assignee.username === member.username)).length}
                     </ListGroup.Item>
                     <ListGroup.Item variant="dark">
                        Unit Tests: {member.unitTests}</ListGroup.Item>
                  </ListGroup>
                  </Card>
               );
            })}
         </div>
         <h1 style={{textAlign: "center"}}><a target="_blank" 
            rel="noopener noreferrer" 
            href="https://gitlab.com/elampietti/jobstop/">GitLab</a> Stats</h1>

            <CardDeck>
            <Card bg="success" text="white" 
               className="text-center ml-5 mt-3 mb-3" style={{ width: '20rem'}}>
               <Card.Body><h5>Commits: {commits.length}</h5></Card.Body>
            </Card>
            <Card bg="success" text="white" className="text-center mt-3 mb-3" 
               style={{ width: '20rem'}}>
               <Card.Body><h5>Issues: {issues.length}</h5></Card.Body>
            </Card>
            <Card bg="success" text="white" 
               className="text-center mr-5 mt-3 mb-3" style={{ width: '20rem'}}>
               <Card.Body><h5>Unit Tests: 62</h5></Card.Body>
            </Card>
         </CardDeck>

         <h1 style={{textAlign: "center"}}>Data</h1>
            
         <CardDeck>
            <Card border="dark" className="text-center ml-5 mt-3 mb-3" 
               style={{ width: '30rem'}}>
            <Card.Header as="h5">Housing</Card.Header>
            <Card.Body>
               <Card.Text>
                  Quandl has a very easy to use python package for its api. 
                  Using the package, you can make requests to the api. The 
                  Quandl dataset we used was Zillow Research Data. You can 
                  specify a type of region and an indicator (eg: Median price) 
                  and relevant data is returned.
               </Card.Text>
               <Button variant="primary" 
                  href="https://www.quandl.com/data/ZILLOW-Zillow-Real-Estate-\Research?keyword=ny" 
                  target="_blank">Quandl</Button>
            </Card.Body>
            </Card>
            <Card border="dark" className="text-center mt-3 mb-3" style={{ width: '30rem'}}>
            <Card.Header as="h5">Climate</Card.Header>
            <Card.Body>
               <Card.Text>
                  For climate, we used two APIs, one for current forecast and 
                  one for historical data. We used OpenWeatherMap to request 
                  current weather information, which gave us the current 
                  temperature, weather icon, humidity, and wind speed. We used a
                  second API, Meteostat, to get the average high and low 
                  temperatures and the yearly precipitation. Both APIs accept 
                  the name of a city as input and respond with the information 
                  in a JSON format.
               </Card.Text>
               <Button variant="primary" 
                  href="https://dev.meteostat.net/api/stations/climate" 
                  target="_blank">OpenWeatherMap</Button>
            </Card.Body>
            </Card>
            <Card border="dark" className="text-center mt-3 mb-3 mr-5" 
               style={{ width: '30rem'}}>
            <Card.Header as="h5">Jobs</Card.Header>
            <Card.Body>
               <Card.Text>
                  Adzuma allows for an API call with a specific job keywords and
                  a location. The response is a JSON file that contains a list 
                  of jobs that meet the specification and additional details for
                  each job. From the list we were able to scrape the company 
                  name, job title, and job description.
               </Card.Text>
               <Button variant="primary" href="https://developer.adzuna.com/" 
                  target="_blank">Adzuma</Button>
            </Card.Body>
            </Card>
         </CardDeck>

         <h1 style={{textAlign: "center"}}>Tools</h1>
         <CardColumns className="text-center mt-5 mb-5 mr-5 ml-5"> 
         {tools.map((tool, index) => {
               return (
                  <Card border="dark" key={index}>
                  <Card.Img variant="top" src={tool.src}/>
                  <Card.Body>
                     <Card.Text>
                        {tool.text}
                     </Card.Text>
                     <Button variant="primary" href={tool.link} 
                        target="_blank">{tool.name}</Button>
                  </Card.Body>
                  </Card>
               );
            })}
         </CardColumns>
      </div>
   );
}
 
export default About;