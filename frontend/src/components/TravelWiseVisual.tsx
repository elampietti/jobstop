import React from 'react';
import SafetyVisual from './Visualizations/SafetyVisual';
import AirportVisual from './Visualizations/AirportVisual';
import CovidVisual from './Visualizations/CovidVisual';
import { Typography } from '@material-ui/core';

function TravelWiseVisual () {
    return (
        <div>
            <Typography variant="h2" align="center">TravelWise Visualizations</Typography>
            <Typography variant="h3" align="center">
                Top 15 Safest Cities in the World
            </Typography>
            <SafetyVisual />
            <Typography variant="h3" align="center">
                Airports by Time Zone (UTC)
            </Typography>
            <AirportVisual />
            <Typography variant="h3" align="center">
                Countries' New COVID-19 Cases vs Total COVID-19 Cases
            </Typography>
            <CovidVisual />
        </div>
    );
}

export default TravelWiseVisual;