import { TableBody } from '@material-ui/core';
import React from 'react';
import GetTable from './tableContainer';

type InstanceTableProps = {
    rawRecords: any,
    dispRecords: Function,
    headCells: any,
}

export function InstanceTable(props: InstanceTableProps){
  
    // obtain the Table elements
    const {
       TblContainer,
       TblHead,
    } = GetTable(props.rawRecords, props.headCells);

    return (
        <div>
          <TblContainer>
             <TblHead/>
             <TableBody>
                {props.dispRecords(props.rawRecords)}
             </TableBody>
          </TblContainer>
        </div>
    )
 }
 