import { TableCell, TableRow } from '@material-ui/core';
import React from 'react';
import { InstanceTable } from './InstanceTable';
import { InstanceGridProps } from './InstanceGridProps';

export default function MLPGrid(props: InstanceGridProps){
    // column names in the table
    const headCells = [
        {id:'MLPCC', label:'Condo/Co-op', disableSorting:true},
        {id:'MLPDT', label:'Duplex/Triplex', disableSorting:true},
        {id:'MLP5R', label:'5 bedroom', disableSorting:true},
        {id:'MLP4R', label:'4 bedroom', disableSorting:true},
        {id:'MLP3R', label:'3 bedroom', disableSorting:true},
        {id:'MLP2R', label:'2 bedroom', disableSorting:true},
        {id:'MLP1R', label:'1 bedroom', disableSorting:true},
     ]
     

    /* function converts raw record information (JSON) to react component */
    function recordToElem(records:any){
       return (
           <TableRow>
               {
                   records.map((data:any, idx:number) => {
                        return (
                            <TableCell key={idx}>{data}</TableCell>
                        )
                    })
               }
           </TableRow>
       )
    }
 
    return (
       <InstanceTable rawRecords={props.records} dispRecords={recordToElem}
       headCells={headCells}/>
    )
 
 }
