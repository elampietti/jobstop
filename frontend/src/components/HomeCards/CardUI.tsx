import React from 'react';
import './mycard-style.css'

const MyCard = (props : any) => {
    return (
        <div className="card text-center">
        <div className="overflow">
            <img src={props.imgsrc} alt="" className="card-img-top"/>
        </div>
        <div className="card-body text-dark">
            <a href={props.page} className="btn btn-outline-success">
                {props.title}
            </a>
        </div>
        </div>
    )
}

export default MyCard;