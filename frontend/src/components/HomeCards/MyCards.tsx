import React from 'react'
import MyCard from './CardUI'
import housing from './home/housing.jpg'
import jobs from './home/jobs.jpg'
import climate from './home/climate.jpg'

function MyCards() {
    return (
        <div className="container-fluid d-flex justify-content-center">
            <div className="row">
                <div className="column mr-5 mt-5 mb-5">
                    <MyCard imgsrc={housing} title="Housing" page="/housing"/>
                </div>
                <div className="column mt-5 mb-5">
                    <MyCard imgsrc={climate} title="Climate" page="/climate"/>
                </div>
                <div className="column ml-5 mt-5 mb-5">
                    <MyCard imgsrc={jobs} title="Jobs" page="/jobs"/>
                </div>
            </div>
        </div>
    )
}

export default MyCards;