import React from 'react';
import { Route, Switch } from 'react-router-dom';
import About from './About';
import CityClimate from './CityClimate';
import CityDetails from './CityDetails';
import ClimateNav from './ClimateNav';
import Home from './Home';
import HousingNav from './HousingNav';
import JobDetails from './JobDetails';
import JobNav from './JobNav';
import SearchResults from './SearchResults';
import Visual from './Visual';
import TravelWiseVisual from './TravelWiseVisual';

const routes = [
    {
        path : '/',
        comp : Home,
    },
    {
        path : '/about',
        comp : About,
    },
    {
        path : '/housing',
        comp : HousingNav,
    },
    {
        path : '/climate',
        comp : ClimateNav,
    },
    {
        path : '/jobs',
        comp : JobNav,
    },
    {
        path : '/visual',
        comp : Visual,
    },
    {
        path : '/providervisual',
        comp : TravelWiseVisual,
    },
    {
        path : '/housing/city_details/:city_idx',
        comp : CityDetails,
    },
    {
        path : '/climate/city_climate/:city_idx',
        comp : CityClimate,
    },
    {
        path : '/jobs/job_details/:id' ,
        comp : JobDetails,
    },
    {
        path : '/search_results/:search',
        comp : SearchResults,
    }
]

export default function AppRoutes(){
    return (
        <div>
                <Switch>
                    {routes.map((route, index:number) => {
                        return (
                            <Route
                             exact path={route.path}
                             component={route.comp}
                             key={index}/>
                        );
                    })}
                    <Route render={function () {
                        return <p>Not found</p>
                    }} />
                </Switch>
            </div>
    )
}