import { Typography } from '@material-ui/core'
import React from 'react'

export default function Error(){
    return (
        <div>
            <Typography variant="h2" align="center">
                We have encountered some errors loading the page. Please reload
            </Typography>
        </div>
    )
}