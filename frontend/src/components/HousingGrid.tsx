import React from 'react';
import HousingGridElement from './HousingGridElement';
import { ModelGridProps } from './ModelGridProps';
import { ModelTable } from './ModelTable';
 
 export default function HousingGrid(props: ModelGridProps){
    // column names in the table
    const headCells = [
       {id:'cityName', label:'City', canSearch:true},
       {id:'MLPAH', label:'Median Listing Price in $'},
       {id:'MLPFAH', label:'Median Listing Price per sq ft in $'},
       {id:'MRPAH', label:'Median Rental Price in $'},
       {id:'MRPFAH', label:'Median Rental Price per sq ft in $'},
       {id:'PHDVAH', label:'% of homes decreasing in value'},
       {id:'PHIVAH', label:'% of homes increasing in value'},
       {id:'moreDetails', label:'More details', disableSorting:true},
       {id:'climateDetails', label:'Climate data', disableSorting:true}
    ]
 
    /* function converts raw record information (JSON) to react component */
    function recordToElem(records:any){
       return (
          records.map((record:any, index:number) => {
             return(
                <HousingGridElement {...record} key={index}/>
             )
          })
       )
    }
 
    return (
       <ModelTable rawRecords={props.records} dispRecords={recordToElem}
         headCells={headCells} searchFn={props.searchFn} 
         filterFn={props.filterFn}/>
    )
 
 }