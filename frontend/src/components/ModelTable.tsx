import { TableBody } from '@material-ui/core';
import React from 'react';
import GetTable from './tableContainer';

type ModelTableProps = {
    rawRecords: any,
    dispRecords: Function,
    headCells: any,
    searchFn: any,
    filterFn: any,
}

export function ModelTable(props: ModelTableProps){
  
    // obtain the Table elements
    const {
       TblContainer,
       TblHead,
       TblPagination,
       recordsAfterPaging
    } = GetTable(props.rawRecords, props.headCells, props.searchFn, 
         props.filterFn);

    return (
        <div>
          <TblContainer>
             <TblHead/>
             <TableBody>
                {props.dispRecords(recordsAfterPaging())}
             </TableBody>
          </TblContainer>
          <TblPagination/>  
        </div>
    )
 }
 