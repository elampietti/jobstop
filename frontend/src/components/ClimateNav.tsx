import React from 'react';
import './Model.css';
import { useState, useEffect } from 'react';
import { Button, Dialog,
         ListItem, Typography } from '@material-ui/core';
import FilterDialog from './FilterDialog';
import ModelSearchBar from './ModelSearchBar';
import ClimateGrid from './ClimateGrid';
import useAxios from 'axios-hooks';
import LoaderIcon from './LoadingIcon';
import Error from './Error';

function ClimateNav() {

    /* api request setup */
    const [{ data, loading, error }] = useAxios(
        {
            url: 'https://api.jobstop.me/climate?by=summary'
        }
    )

    const [allRecords, setAllRecords] = useState([] as any);
    /* for search */
    const [searchFn, setSearchFn] = useState(
                                    {fn: (items: any) => {return items}, str:''}
                                 );

    /* for numerical filters */
    // if true, the dialog box is displayed
    const [open, setOpen] = useState(false);
    // an object storing a list of all the filters being applied
    const[numFilters, setNumFilters] = useState({filters: [] as any})
    // the columns containing numerical values we can filter on
    const numColumns = ['lowTemp', 'highTemp', 'precipitation', 
        'typicalHumidity', 'typicalWindSpeed'];
    // the ranges of each of the columns -> maybe make dynamic
    const ranges = {
      lowTemp: [-20, 20],
      highTemp: [10, 45],
      precipitation: [80, 2200],
      typicalHumidity: [40, 90],
      typicalWindSpeed: [0, 25]
    }

    /* for displaying created filters */
    const[filterLabels, setFilterLabels] = useState([] as any);
    const[alert, setAlert] = useState(false);
    const[view, setView] = useState(false);
   
    // on button click, display the dialog
    const handleClickOpen = () => {
        setOpen(true);
    };

    // function handles when a filter is added
    function handleFilterAdd (col="", range=[]){
        // close the dialog
        setOpen(false);
        // ensure a valid column was provided
        if (range.length !== 0 && col !== ""){
            let addFilter = true;
            for (var label of filterLabels){
                if (label[0] === col){
                    addFilter = false
                    setAlert(true);
                }
            }
            if (addFilter){
                // get the current filters.
                let currentFilters:Array<Function> = numFilters.filters;
                // create the new filter 
                let newFilter:Function = (items: any) => {
                    return items.filter(
                        (x:any) => x[col] >= range[0] && x[col] <= range[1]
                        );
                }
                // update the filters and the filter labels
                currentFilters.push(newFilter);
                setNumFilters({filters: currentFilters});
                let newFilterLabels:any = filterLabels;
                let rangeString = '' + range[0] + ' to ' + range[1]
                filterLabels.push([col, rangeString])
                setFilterLabels(newFilterLabels);
            }
        
        }
    };

    // if the clear button is clicked, remove all filters 
    const handleClear = () => {
        setNumFilters({filters: []});
        setFilterLabels([])
    }

    // set the filters dialog to open
    function displayFilters(){
        setView(true);
    }

    // handle searches on cities and states
    const handleSearch = (e:any) => {
        let target = e.target;
        setSearchFn({
           fn: (items:any) => {
              if (target.value === "")
                 return items;
              else
                 return items.filter((x:any) => 
                    x['cityName'].includes(target.value))
           },
           str: target.value
        })
     }

    // construct the content of the table
    useEffect(() => {

        if (!loading){
            // initialize information needed to organize data
            let myRecords = [];
            let currentDate = new Date();
            let currentMonth = currentDate.getMonth();

            // push each row into a list
            for (var key in data) {
                let climateData = data[key];
                let lTemp = Math.min(...climateData['lowTemps']);
                let hTemp = Math.max(...climateData['highTemps']);
                let prec = climateData['precipitation'].reduce
                (
                    function (a:number, b: number) { return a + b; }
                );
                myRecords.push(
                    {
                        cityName:String(key),
                        lowTemp:lTemp,
                        highTemp:hTemp,
                        precipitation: prec,
                        typicalHumidity:
                            climateData['humidities'][currentMonth],
                        typicalWindSpeed:Number
                        (
                            climateData['windSpeeds'][currentMonth]
                        ),
                        cityId: Number(climateData['cityId'])
                    }
                )
            }

            // construct the table using the list of rows
            setAllRecords(myRecords)
        }
    }, [loading, data])

    /* display spinner while loading */
    if (loading){
        return (
            <LoaderIcon/>
        )
    }  

    /* error message is displayed if fetching doesn't work */
    if (error){
        return (  
            <Error/>
        ) 
    }

    let buttonText = ["Add Filter", "Clear Filters", "View Filters"];
    let buttonFunc = [handleClickOpen, handleClear, displayFilters];

    return (
        <div className="model">
            <Typography variant="h2" align="center">Climate Data</Typography>
            <div className="model-content">
            <div className="row">
               <div className="col-md-3">
                  <ModelSearchBar label={"search cities/state"}
                   changeFn={handleSearch} />
               </div>
               {
                   buttonText.map((text: string, index:number) => {
                       return (
                           <div className="col-sm-2" key={index}>
                                <Button variant="outlined"
                                onClick={buttonFunc[index]}
                                fullWidth={true}>
                                    {text}
                            </Button>   
                            </div>
                       )
                   })
                }
                <div className="col">
                    <Dialog open={view} onClose={()=>{setView(false)}}
                    maxWidth="sm" fullWidth={true}>
                        {filterLabels.map((filter:any, idx:number) => {
                            return (
                            <ListItem key={idx}>{filter[0]+': '+
                                filter[1]}</ListItem>
                            )
                        })}
                    </Dialog>
                    <Dialog open={alert} onClose={()=>setAlert(false)}
                    maxWidth="sm" fullWidth={true}>
                        Can't add filter
                    </Dialog>
                </div>
                </div>
                <FilterDialog  
                ranges={ranges}
                columns={numColumns}
                popupOpen={open}
                onClose={handleFilterAdd}
                />
                <div className="model-table">
                    <ClimateGrid
                    records={allRecords}
                    filterFn={numFilters}
                    searchFn={searchFn}
                    />
                </div>
            </div>
        </div>
    );
}

export default ClimateNav;