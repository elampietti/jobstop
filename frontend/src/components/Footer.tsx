import React from 'react';
import './Footer.css'

export default function Footer(){

    let link=["https://www.quandl.com/data/ZILLOW-Zillow-Real-Estate-Research",
              "https://dev.meteostat.net",
              "https://developer.adzuna.com/",
              "https://clearbit.com"
              ];
    let text=["Quandl", "Meteostat", "Adzuma", "Logos Provided by Clearbit"];
    return(
        <div className="main-footer">
          <div className="container">
              <div className="row">
                <div className="col">
                  <p>Sources:&nbsp;
                    {
                      link.map((myLink:string, index:any) => {
                        return (
                            <a target="_blank" rel="noopener noreferrer"
                             href={myLink} style={{color: "white"}}
                             key={index}>
                              {text[index]+" | "}
                            </a>
                        )
                      })
                    }
                  </p>
                  <p>
                    <a href="/about" style={{color:'white'}}>About Us</a>
                  </p>
                  <p>
                    Need help: help@jobstop.me
                  </p>
                </div>
              </div>
              <hr/>
              <div className="row">
                <p className="col-sm">
                  &copy;2020 JobStop | All Rights Reserved 
                </p>
              </div>
          </div>
        </div>
    )
}