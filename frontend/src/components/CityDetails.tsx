/* imports */
import React, { useEffect, useState } from 'react';
import './Model.css';
import './Housing.css'
import { Chart } from 'react-google-charts';
import { Button, Typography } from '@material-ui/core';
import climate from './TransitionImages/climate.svg'
import useAxios from 'axios-hooks';
import LoaderIcon from './LoadingIcon';
import MRPGrid from './MRPGrid';
import MLPGrid from './MLPGrid';
import JobGrid from './JobGrid';
import Error from './Error';


function CityDetails(props: any) {
    /* these props are supplied from the link */
    const cityIdx = props.match.params.city_idx;
    /*const city = props.match.params.city_name;*/
    const [city, setCity] = useState('')
    /* state variables and associated set functions */
    const [coord, setCoord] = useState("");
    const [ZHVIData, setZHVIData] = useState([] as any)
    const [ZRVIData, setZRVIData] = useState([] as any)
    const [MLPData, setMLPData] = useState([] as any)
    const [MRPData, setMRPData] = useState([] as any)
    const [jobRecords, setJobRecords] = useState([] as any);

    /* api request setup */
    const [{ data: result, loading, error }] = useAxios(
        {
            url: 'https://api.jobstop.me/cities/cityData?cityIdx=' + cityIdx
        }
    )

    /* empty filters to display job grid */
    const searchFn = { fn: (items: any) => { return items }, str: '' };
    const filterFn = { filters: [] };

    /* runs after the component is mounted */
    useEffect(() => {

        if (!loading) {
            /* obtain data from results and set the appropriate variable */
            setMLPData(result['mlp'])
            setMRPData(result['mrp'])
            setZHVIData(result['ZHVIAH'])
            setZRVIData(result['ZRVIAH'])
            setCoord("https://www.google.com/maps/embed/v1/view?key=" +
                process.env.REACT_APP_GOOGLE_MAP_API_KEY + "&center=" +
                result['coord']['latitude'] + "," +
                result['coord']['longitude'] + "&zoom=13")

            /* collecting job data */
            let jobsData = result['jobs']
            let cityNameSet = false;
            let myRows = [];
            for (var key in jobsData) {
                let jobData = jobsData[key];
                if (!cityNameSet) {
                    setCity(jobData['location']);
                    cityNameSet = true;
                }
                let jobRecord = {
                    jobID: Number(key),
                    ...jobData
                }
                myRows.push(
                    jobRecord
                )
            }
            setJobRecords(myRows)
        }
    }, [loading, result])

    /* display spinner while loading */
    if (loading) {
        return (
            <LoaderIcon />
        )
    }

    /* error message is displayed if fetching doesn't work */
    if (error) {
        return (
            <Error />
        )
    }

    return (
        <div className="housing">
            <div>
                <Typography variant="h3" align="center">
                    {String(city)} Housing
                </Typography>
            </div>
            <div className="map">
                <iframe
                    title="map-view"
                    src={coord}
                    width="100%"
                    height="450"
                    frameBorder="0"
                    style={{ border: '0' }}
                    allowFullScreen
                    aria-hidden="false" >
                </iframe>
                <h3>Climate Data:</h3>
                <Button
                    variant="outlined"
                    href={'/climate/city_climate/' + cityIdx}
                >
                    <img src={climate} className="climate-photo" alt=''/>
                </Button>
            </div>

            <div className="table-box">
                <br></br>
                <h3>Median Listing Price By House Types (most recent)</h3>
                <MLPGrid records={MLPData} />
                <br></br>
                <h3>Median Rental Price By House Types (most recent)</h3>
                <MRPGrid records={MRPData} />
                <br></br>
            </div>

            <div className="graphs">
                <div className="d-flex">
                    <div className="graph">
                        <h3>Zillow Home Value Index over time</h3>
                        <Chart
                            height={'500px'}
                            chartType='LineChart'
                            loader={<div>Loading ZHVIAH data...</div>}
                            data={ZHVIData}
                            options={{
                                hAxis: {
                                    title: 'Month'
                                },
                                vAxis: {
                                    title: 'Zillow Home Value Index'
                                },
                            }}
                        />
                    </div>
                    <div className="graph">
                        <h3>Zillow Rental Value Index over time</h3>
                        <Chart
                            height={'500px'}
                            chartType='LineChart'
                            loader={<div>Loading ZRIAH data...</div>}
                            data={ZRVIData}
                            options={{
                                hAxis: {
                                    title: 'Month'
                                },
                                vAxis: {
                                    title: 'Zillow Rental Value Index'
                                },
                            }}
                        />
                    </div>
                </div>
            </div>

            <div className='table-box'>
                <h3>Jobs in the area:</h3>
                <div className="model-table">
                    <JobGrid
                        records={jobRecords}
                        searchFn={searchFn}
                        filterFn={filterFn}
                    />
                </div>
            </div>
        </div>
    );
}

export default CityDetails;