import React, { useState, useEffect } from 'react';
import {
    BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip
  } from 'recharts';
import useAxios from 'axios-hooks';
import LoaderIcon from '../LoadingIcon';
import Error from '../Error';

export default function FirstVisual () {

  const [{ data, loading, error }] = useAxios(
    {
        url: 'https://api.jobstop.me/cities/summary'
    }
  )

  const[cityData, setCityData] = useState(
    [] as any
  )

  useEffect(() => {

    if(!loading){
      let myData = Object.values(data);
      let keys = Object.keys(data);
      for(let i = 0; i < keys.length; i++){
        Object.assign(myData[i], {name: keys[i]})
      }
      myData.sort(compare);
      for(let i = 0; i < 90; i++){
        myData.pop();
      }
      setCityData(myData);
    }

  }, [data, loading])


  function compare(a: any, b: any ){
    if(a.MLPAH > b.MLPAH){
      return -1
    }
    return 1
  }

  if(loading){
    return (
      <LoaderIcon/>
    )
  }

  if(error){
    return (
      <Error/>
    )
  }

  return (
      <BarChart
      width={1000}
      height={600}
      data={cityData}
      layout="vertical"
      margin={{
        top: 5, right: 30, left: 200, bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis type="number"/>
      <YAxis type="category" dataKey="name"/>
      <Bar dataKey="MLPAH" fill="#8884d8" />
      <Tooltip />
    </BarChart>
  );
}