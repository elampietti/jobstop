import React, { useEffect, useState } from 'react';
import {
    ScatterChart, Scatter, XAxis, YAxis, ZAxis, CartesianGrid, Tooltip
  } from 'recharts';
import useAxios from 'axios-hooks';
import LoaderIcon from '../LoadingIcon';
import Error from '../Error';

export default function ClimateVisual (){

    const [{ data, loading, error }] = useAxios(
      {
         url: 'https://api.jobstop.me/climate?by=summary'
      }
    )
    const[climateData, setClimateData] = useState(
      [] as any
    )
    useEffect(() => {

      if (!loading) {
        let mydata = [];
        for(var key in data){
          let entry = data[key];
          mydata.push(
            {
              name: key,
              precipitation: entry.precipitation.reduce(
                (a:number, b:number) => a+b, 0) / entry.precipitation.length,
              windspeed: entry.windSpeeds.reduce(
                (a:number, b:number) => a+b, 0) / entry.windSpeeds.length
            }
          )
        }
        setClimateData(mydata);
      }
   }, [data, loading])

   

  if(loading){
    return (
      <LoaderIcon/>
    )
  }

  if(error){
    return(
      <Error/>
    )
  }
  
  return (
      <ScatterChart
      width={1000}
      height={600}
      data={climateData}
      margin={{
        top: 5, right: 30, left: 200, bottom: 5,
      }}
    >
      <CartesianGrid />
      <XAxis type="number" dataKey="precipitation" name="Precipitation"
       unit="in"/>
      <YAxis type="number" dataKey="windspeed" name="WindSpeed" unit="mph" />
      <ZAxis type="category" dataKey="name" name="City"/>
      <Tooltip />
      <Scatter name="A school" data={climateData} fill="#8884d8" />
    </ScatterChart>
  );
}