import React, { useEffect, useState } from 'react';
import {
    PieChart, Pie, Tooltip, Cell
  } from 'recharts';
import useAxios from 'axios-hooks';
import LoaderIcon from '../LoadingIcon';
import Error from '../Error';

export default function AirportVisual (){

  const [{ data, loading, error }] = useAxios(
    {
        url: 'https://api.travelwise.live/airports'
    }
  )
  const[airportData, setAirportData] = useState(
    [] as any
  )
  useEffect(() => {

    if (!loading) {
      let mydata: { [key: string]: number; } = {};
      for(var key in data){
        let entry = data[key];
        if(mydata.hasOwnProperty(entry.time_offset)){
          mydata[entry.time_offset as string] = 
          mydata[entry.time_offset as string] + 1;
        } else {
          mydata[entry.time_offset as string] = 1;
        }
      }
      let dataArray = [];
      for(var key1 in mydata){
          dataArray.push(
              {
                  time_offset: key1,
                  airports: mydata[key1]
              }
          )
      }
      dataArray.sort(compare)
      setAirportData(dataArray);
    }
  }, [data, loading])

  const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

  function compare(a: any, b: any ){
    if(a.time_offset > b.time_offset){
      return -1
    }
    return 1
  }

  if(loading){
    return (
      <LoaderIcon/>
    )
  }

  if(error){
    return (
      <Error/>
    )
  }

  return (
    <PieChart width={1000} height={600} margin={{
      top: 5, right: 30, left: 400, bottom: 5,
    }}>
        <Pie dataKey="airports" data={airportData} cx={200} cy={200}
         outerRadius={200}
         nameKey="time_offset" >
          {
            airportData.map((time_offset: string, airports :number) =>
             <Cell key={`cell-${time_offset}`}
              fill={COLORS[airports % COLORS.length]} />
            )
          }
        </Pie>
      <Tooltip />
    </PieChart>
  );
}