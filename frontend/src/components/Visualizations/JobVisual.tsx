import React, { useEffect, useState } from 'react';
import {
    PieChart, Pie, Tooltip, Cell
  } from 'recharts';
import useAxios from 'axios-hooks';
import LoaderIcon from '../LoadingIcon';
import Error from '../Error';

export default function JobVisual (){

  const [{ data, loading, error }] = useAxios(
    {
        url: 'https://api.jobstop.me/jobs?by=summary'
    }
  )
  const[jobData, setJobData] = useState(
    [] as any
  )
  useEffect(() => {

    if (!loading) {
      let mydata: { [key: string]: number; } = {};
      for(var key in data){
        let entry = data[key];
        if(mydata.hasOwnProperty(entry.category)){
          mydata[entry.category as string] = 
          mydata[entry.category as string] + 1;
        } else {
          mydata[entry.category as string] = 1;
        }
      }
      let dataArray = [];
      for(var key2 in mydata){
          dataArray.push(
              {
                  category: key2,
                  listings: mydata[key2]
              }
          )
      }
      setJobData(dataArray);
    }
  }, [data, loading])

  const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

  if(loading){
    return (
      <LoaderIcon/>
    )
  }

  if(error){
    return(
      <Error/>
    )
  }

  return (
      <PieChart width={1000} height={600} margin={{
        top: 5, right: 30, left: 400, bottom: 5,
      }}>
        <Pie dataKey="listings" data={jobData} cx={200} cy={200}
         outerRadius={200}
         nameKey="category" >
          {
            jobData.map((entry:any, index:number) =>
             <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
            )
          }
        </Pie>
      <Tooltip />
    </PieChart>
  );
}