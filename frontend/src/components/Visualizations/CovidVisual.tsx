// scatterplot total_cases vs new cases
import React, { useEffect, useState } from 'react';
import {
    ScatterChart, Scatter, XAxis, YAxis, ZAxis, CartesianGrid, Tooltip
  } from 'recharts';
import useAxios from 'axios-hooks';
import LoaderIcon from '../LoadingIcon';
import Error from '../Error';

export default function CovidVisual (){

  const [{ data, loading, error }] = useAxios(
    {
        url: 'https://api.travelwise.live/covid'
    }
  )
  const[covidData, setCovidData] = useState(
    [] as any
  )
  useEffect(() => {

    if (!loading) {
      let myData = [];
      for(var key in data){
        let entry = data[key];
        myData.push(
          {
            name: String(entry.country),
            newCases: Number(entry.new_cases),
            totalCases: Number(entry.total_cases)
          }
        )
      }
      setCovidData(myData);
    }
  }, [data, loading])

  if(loading){
    return (
      <LoaderIcon/>
    )
  }

  if(error){
    return(
      <Error/>
    )
  }
  
  return (
      <ScatterChart
      width={1000}
      height={600}
      data={covidData}
      margin={{
        top: 5, right: 30, left: 200, bottom: 5,
      }}
      >
      <CartesianGrid />
      <XAxis type="number" dataKey="totalCases" name="Total Cases" />
      <YAxis type="number" dataKey="newCases" name="New Cases" />
      <ZAxis type="category" dataKey="name" name="City"/>
      <Tooltip />
      <Scatter name="scatterplot" data={covidData} fill="#8884d8" />
    </ScatterChart>
  );
}