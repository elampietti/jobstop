import React from 'react';
import ClimateGridElement from './ClimateGridElement';
import { ModelGridProps } from './ModelGridProps';
import { ModelTable } from './ModelTable';

export default function ClimateGrid(props: ModelGridProps) {

    // initialize the column headers for the table
    const headCells = [
        { id: 'cityName',
            label: 'City', canSearch:true},
        { id: 'lowTemp', 
            label: 'Coldest Month\'s Average Low in Degrees Celsius' },
        { id: 'highTemp', 
            label: 'Hottest Month\'s Average High in Degrees Celsius' },
        { id: 'precipitation',
            label: 'Annual Precipitation' },
        { id: 'typicalHumidity',
            label: 'Current Month\'s Typical Humidity' },
        { id: 'typicalWindSpeed',
            label: 'Current Month\'s Typical Wind Speed' },
        { id: 'moreDetails',
            label: 'More Details', disableSorting:true },
        { id: 'housingDetails',
            label: 'Housing Details', disableSorting:true },
    ]

    /* function converts raw record information (JSON) to react component */
    function recordToElem(records:any){
        return (
           records.map((record:any, index:number) => {
              return(
                 <ClimateGridElement {...record} key={index}/>
              )
           })
        )
     }
  
     return (
        <ModelTable rawRecords={props.records} dispRecords={recordToElem}
            headCells={headCells} searchFn={props.searchFn} 
            filterFn={props.filterFn}/>
     )
}