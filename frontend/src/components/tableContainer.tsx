import React, {useState} from 'react';
import {Table, TableCell, TableHead, TablePagination,
        TableRow, TableSortLabel, Button} from '@material-ui/core';
import { makeStyles, useTheme, Theme,
         createStyles } from '@material-ui/core/styles';
import {FirstPage, KeyboardArrowLeft, KeyboardArrowRight,
        LastPage} from '@material-ui/icons';


export default function GetTable(records:any, headCells:any,
                        filterFn = {fn:(items: any) => {return items}, str:''},
                        numFilters ={ filters: []}){
    /* controls how many rows are displayed */
    const opRowsPerPage = [5, 10, 25];
    /* variables for pagination */
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(opRowsPerPage[0]);
    const [order, setOrder] = useState('' as any);
    const [orderBy, setOrderBy] = useState();
    
    /* container for the entire table */
    const TblContainer = (props: any) => (
        <Table>
            {props.children}
        </Table>
    )
    
    /* Table head */
    const TblHead = (props: any) => {

        /* figure out the sorting order */
        const handleSortRequest = (cellId:any) => {
            const isAsc = orderBy === cellId && order === "asc";
            setOrder(isAsc?"desc":"asc");
            setOrderBy(cellId);
        }

        return (
            <TableHead>
                <TableRow>
                    {headCells.map((headCell: any) => (
                        <TableCell key={headCell.id}
                        sortDirection={orderBy === headCell.id ? order : false}>
                            {headCell.disableSorting?headCell.label: 
                                <TableSortLabel
                                active={orderBy === headCell.id}
                                direction={orderBy === headCell.id?order:"asc"}
                                onClick={() => {handleSortRequest(headCell.id)}}
                                >
                                    {headCell.label}
                                </TableSortLabel>
                            } 
                         
                        </TableCell>
                    ))}
                </TableRow>
            </TableHead>
        )
    }

    const useStyles1 = makeStyles((theme: Theme) =>
    createStyles({
        root: {
        flexShrink: 0,
        marginLeft: theme.spacing(2.5),
        },
        
    }),
    );

    /* props used for the table view options component */
    interface TablePaginationActionsProps {
        count: number;
        page: number;
        rowsPerPage: number;
        onChangePage: (
            event: React.MouseEvent<HTMLButtonElement>, newPage: number)
            => void;
    }

    /* define actions associated with clicking buttons and return those html
    elements */
    function TablePaginationActions(props: TablePaginationActionsProps) {
        const classes = useStyles1();
        const theme = useTheme();
        const { count, page, rowsPerPage, onChangePage } = props;

        const handleFirstPageButtonClick =
        (event: React.MouseEvent<HTMLButtonElement>) => {
            onChangePage(event, 0);
        };

        const handleBackButtonClick =
        (event: React.MouseEvent<HTMLButtonElement>) => {
            onChangePage(event, page - 1);
        };

        const handleNextButtonClick =
        (event: React.MouseEvent<HTMLButtonElement>) => {
            onChangePage(event, page + 1);
        };

        const handleLastPageButtonClick =
        (event: React.MouseEvent<HTMLButtonElement>) => {
            onChangePage(event, Math.max(0, Math.ceil(count/rowsPerPage) - 1));
        };

        return (
            <div className={classes.root}>
            <Button
                onClick={handleFirstPageButtonClick}
                disabled={page === 0}
                aria-label="first page"
            >
                {theme.direction === 'rtl' ? <LastPage /> : <FirstPage />}
            </Button>
            <Button onClick={handleBackButtonClick} disabled={page === 0}
             aria-label="previous page">
                {
                    theme.direction === 'rtl'
                    ? <KeyboardArrowRight /> : <KeyboardArrowLeft />
                }
            </Button>
            <Button
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="next page"
            >
                {
                    theme.direction === 'rtl' ?
                    <KeyboardArrowLeft /> : <KeyboardArrowRight />
                }
            </Button>
            <Button
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="last page"
            >
                {theme.direction === 'rtl' ? <FirstPage /> : <LastPage />}
            </Button>
            </div>
        );
    }

    const handleChangePage = (event: any, newPage:any) =>{
        setPage(newPage);
    }

    const handleChangeRowsPerPage = (event:any) =>{
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    }

    /* entire pagination component */
    const TblPagination = () => (
        <TablePagination
         ActionsComponent={TablePaginationActions}
         component="div"
         page={page} 
         rowsPerPageOptions={opRowsPerPage}
         rowsPerPage={rowsPerPage}
         count={records.length}
         onChangePage= {handleChangePage}
         onChangeRowsPerPage = {handleChangeRowsPerPage}
        />
    )

    /* sorting function */
    function stableSort(array: any, comparator:any){
        /* map the records array to [idx, record] */
        const stabilizedThis = array.map((elem:any, idx:any) => [elem, idx]);
        /* use comparator to sort */
        stabilizedThis.sort((a:any, b:any) => {
            const order = comparator(a[0], b[0]);
            if (order !== 0) return order;
            return a[1] - b[1];
        }); 
        return stabilizedThis.map((elem:any) => elem[0])
    }

    /* provides comparators for the records */
    function getComparator(order: any, orderBy: any){
        /* ascending comparator is simply inverse of descending */
        return order === 'desc'
            ? (a: any, b: any) => descendingComparator(a, b, orderBy)
            : (a: any, b: any) => -descendingComparator(a, b, orderBy)
    }

    function descendingComparator(a:any, b:any, orderBy:any){
        if(b[orderBy] < a[orderBy]){
            return -1;
        }
        if(b[orderBy] > a[orderBy]){
            return 1
        }
        return 0;
    }

    /* function returns the appropriate records after filtering and sorting */
    const recordsAfterPaging = () =>{
        let myRecords = records;
        /* apply numerical filters first */
        numFilters.filters.forEach((f:Function) => {myRecords = f(myRecords)})
        /* sort them if any sort is requested */
        myRecords = stableSort(
                filterFn.fn(myRecords),
                getComparator(order, orderBy)
            )
        .slice(page*rowsPerPage, (page+1)*rowsPerPage);
        /* check if highlighting needs to be applied */
        let finalRecords:any = [];
        if(filterFn.str !== ''){
            myRecords.forEach((record:any) => {
                let newRecord = {
                    ...record
                }
                headCells.forEach(function(cell:any){
                    if (cell.canSearch){
                        let hTerm = record[cell.id as string].replace(
                            new RegExp(filterFn.str, 'gi'),
                            (match:any) =>
                             `<mark style="background: #2769AA; color: white;">
                             ${match}</mark>`
                        )
                        newRecord[cell.id as string] = hTerm
                    }
                })
                finalRecords.push(newRecord);
            })
            return finalRecords
        }
        return myRecords;
    }
    /* return all the created components */
    return {
        TblContainer,
        TblHead,
        TblPagination,
        recordsAfterPaging
    }
}