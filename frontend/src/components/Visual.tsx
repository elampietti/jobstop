import React from 'react';
import FirstVisual from './Visualizations/FirstVisual';
import ClimateVisual from './Visualizations/ClimateVisual';
import JobVisual from './Visualizations/JobVisual';
import { Typography } from '@material-ui/core';

function Visual () {
    return (
        <div>
            <Typography variant="h2" align="center">Visualizations</Typography>
            <Typography variant="h3" align="center">
                Top ten median listing prices (by city)
            </Typography>
            <FirstVisual />
            <Typography variant="h3" align="center">
                Average Windspeed vs Precipitation for all cities
            </Typography>
            <ClimateVisual />
            <Typography variant="h3" align="center">
                Distribution of Job Categories in the database (Hover for info)
            </Typography>
            <JobVisual />
        </div>
    );
}

export default Visual;