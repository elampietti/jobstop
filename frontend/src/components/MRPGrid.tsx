import { TableCell, TableRow } from '@material-ui/core';
import React from 'react';
import { InstanceTable } from './InstanceTable';
import {InstanceGridProps} from './InstanceGridProps'


export default function MRPGrid(props: InstanceGridProps){

    // column names in the table
    const headCells = [
        {id:'MRPCC', label:'Condo/Co-op', disableSorting:true},
        {id:'MRPDT', label:'Duplex/Triplex', disableSorting:true},
        {id:'MRP5R', label:'5 bedroom', disableSorting:true},
        {id:'MRP4R', label:'4 bedroom', disableSorting:true},
        {id:'MRP3R', label:'3 bedroom', disableSorting:true},
        {id:'MRP2R', label:'2 bedroom', disableSorting:true},
        {id:'MRP1R', label:'1 bedroom', disableSorting:true},
    ]
 
    /* function converts raw record information (JSON) to react component */
    function recordToElem(records:any){
       return (
           <TableRow>
               {
                   records.map((data:number, idx:number) => {
                        return (
                            <TableCell key={idx}>{data}</TableCell>
                        )
                    })
               }
           </TableRow>
       )
    }
 
    return (
       <InstanceTable rawRecords={props.records} dispRecords={recordToElem}
       headCells={headCells}/>
    )
 
 }
