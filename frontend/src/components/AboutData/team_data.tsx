import elias_headshot from './images/headshots/elias_headshot.png'
import gokul_headshot from './images/headshots/gokul_headshot.jpg'
import ej_headshot from './images/headshots/ej_headshot.jpg'
import rohan_headshot from './images/headshots/rohan_headshot.png'
import jonathon_headshot from './images/headshots/jonathon_headshot.jpeg'

const team = [
    {
       name : "Elias Lampietti", 
       committer_name: "Elias Lampietti",
       username: "elampietti",
       leader: "Team Lead: Phase 1",
       photo: elias_headshot,
       bio: `Junior CS student at UT Austin. Interested and experienced with 
       building neural networks. Enjoys snowboarding, especially after attending
        high school in Switzerland.`,
       linkedin: "https://www.linkedin.com/in/eliaslampietti/",
       role: "Full-Stack",
       commits: 0,
       issues: 0,
       unitTests: 12
    },
    {
       name : "Gokul Anandaraman", 
       committer_name: "GokulNarayan",
       username: "Gokul_Narayan1",
       leader: "Team Lead: Phase 2",
       role: "Full-Stack",
       bio: `Junior majoring in CS at UT Austin. Interested in the field of 
       AI/ML,and specifically computer vision. Enjoys cooking and hopes to open 
       a food truck at some point in the future.`,
       linkedin: "https://www.linkedin.com/in/gokul-anandaraman-076b501aa/",
       photo: gokul_headshot,
       commits: 0,
       issues: 0,
       unitTests: 14
    },
    {
       name : "EJ Castillo", 
       committer_name: "EJ Castillo",
       username: "ej-castillo",
       leader: "Team Lead: Phase 3",
       linkedin: "https://www.linkedin.com/in/ej-castillo-8676641a1/",
       photo: ej_headshot,
       role: "Full-Stack",
       bio: `Computer science Junior at UT Austin. My main academic interests 
       are ML/AI and systems. Outside of tech, I enjoy watching sports and am a 
       big fan of Longhorn Football`,
       commits: 0,
       issues: 0,
       unitTests: 13
    },
    {
       name : "Jonathon Lowe", 
       committer_name: "Jonathon Lowe",
       username: "jlowe22",
       photo: jonathon_headshot,
       leader: "Team Lead: Phase 4",
       role: "Full-Stack",
       bio: `Junior majoring in Computer Science at UT Austin. Interested in 
       Computer Vision, ML, and the overlap between the two. In my free time I 
       enjoy kayaking and sailing.`,
       linkedin: "https://www.linkedin.com/in/jlowe22/",
       commits: 0,
       issues: 0,
       unitTests: 12
    },
    {
       name : "Rohan Kamath", 
       committer_name: "Rohan Kamath",
       username: "rohan-kamath",
       role: "Full-Stack",
       leader: "Issue Tracker",
       bio: `Junior majoring in computer science at UT Austin. Interested in 
       the field of computer graphics and rendering. Currently learning how to 
       longboard and play the guitar.`,
       photo: rohan_headshot,
       linkedin: "https://www.linkedin.com/in/rohan-kamath-489859156/",
       commits: 0,
       issues: 0,
       unitTests: 11
    }
 ]

export default team;