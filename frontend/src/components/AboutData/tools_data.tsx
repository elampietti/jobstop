import slack_img from './images/logos/Slack.png'
import zoom_img from './images/logos/Zoom.png'
import bootstrap_img from './images/logos/Bootstrap.png'
import gitlab_img from './images/logos/GitLab.jpg'
import aws_img from './images/logos/AWS.png'
import postman_img from './images/logos/Postman.jpg'
import react_img from './images/logos/React.png'

const tools = [
    {
       src : react_img,
       text : `React is a JavaScript front-end framework being used to build UI 
         components for the JobStop website.`,
       link : "https://reactjs.org/",
       name : "React"
    },
    {
       src : aws_img,
       text : `This website is being hosted by AWS Amplify. AWS Amplify is also 
       linked to our GitLab repository so commits to the master branch trigger 
       a build and deployment to the JobStop website. We are also using AWS RDS 
       to host our PostgreSQL database.`,
       link : "https://aws.amazon.com/amplify/",
       name : "AWS"
    },
    {
       src : gitlab_img,
       text : `The code for this project is maintained in a GitLab repository. 
       We also utilize GitLab issues to track TODO items and assign them
       to a member of our team.`,
       link : "https://gitlab.com/elampietti/jobstop/",
       name : "GitLab"
    },
    {
       src : zoom_img,
       text : `Our team uses Zoom several times a week for progress and 
       task-assignment meetings.`,
       link : "https://zoom.us/",
       name : "Zoom"
    },
    {
       src : postman_img,
       text : `We used Postman to create a RESTful API for JobStop`,
       link : "https://documenter.getpostman.com/view/12922422/TVeqdn1V",
       name : "Postman"
    },
    {
       src : slack_img,
       text : `Our team uses Slack for communication and discussion outside of 
       meetings.`,
       link : "https://slack.com/",
       name : "slack"
    },
    {
       src : bootstrap_img,
       text : `Bootstrap is a CSS front-end framework being used to enhance 
       visual aspects of our React components.`,
       link : "https://getbootstrap.com/",
       name : "Bootstrap"
    },
 
 ]

 export default tools;