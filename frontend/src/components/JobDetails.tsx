import React, { useEffect, useState } from 'react';
import { Button } from '@material-ui/core';
import './Job.css';
import climate from './TransitionImages/climate.svg'
import housing from './TransitionImages/housing.svg'
import useAxios from 'axios-hooks';
import LoaderIcon from './LoadingIcon';
import Error from './Error';

function JobDetails(props: any) {

    /* Dynamic variables */
    const jobID = props.match.params.id;
    const [company, setCompany] = useState("");
    const [category, setCategory] = useState("");
    const [time, setTime] = useState("");
    const [type, setType] = useState("");
    const [postDate, setPostDate] = useState("");
    const [desc, setDesc] = useState("");
    const [url, setURL] = useState("");
    const [title, setTitle] = useState("");
    const [loc, setLoc] = useState("");
    const [coord, setCoord] = useState("")
    const [cityIdx, setCityIdx] = useState(0)
    const [domain, setDomain] = useState("")

    /* api request setup */
    const [{ data, loading, error }] = useAxios(
        {
            url: 'https://api.jobstop.me/jobs/jobData?jobIdx=' + jobID
        }
    )

    /* Populate dynamic variables from API */
    useEffect(() => {

        if (!loading) {
            setCompany(data['company'])
            setCategory(data['category'])
            setTime(data['contract_time'])
            setType(data['contract_type'])
            setPostDate(data['created'])
            setDesc(data['description'])
            setURL(data['redirect_url'])
            setTitle(data['title'])
            setLoc(data['location'])
            setCityIdx(data['city_id'])
            setCoord("https://www.google.com/maps/embed/v1/view?key=" +
                process.env.REACT_APP_GOOGLE_MAP_API_KEY + "&center=" +
                data['latitude'] + "," + data['longitude'] + "&zoom=13")
            setDomain(data['domain'])
        }


    }, [loading, data])

    /* Get Logo from Clearbit API */
    const logo = "//logo.clearbit.com/" + domain;

    /* display spinner while loading */
    if (loading) {
        return (
            <LoaderIcon />
        )
    }

    /* error message is displayed if fetching doesn't work */
    if (error) {
        return (
            <div>
                <Error />
            </div>
        )
    }

    return (
        <div className="instance">
            <div className="d-inline-flex p-2">
                <img src={logo} className="logo" alt=""></img>
                <div className="title">
                    <h2>{company}</h2>
                    <h3>{title}</h3>
                    <p>Posted: {postDate}</p>
                </div>
            </div>

            <div className="d-flex">
                <div>
                    <div className="desc">
                        <b>Job Category</b>
                        <p>{category}</p>
                        <b>Job Description</b>
                        <p>{desc}</p>
                        <p>More information here:&nbsp;&nbsp;<u><a href={url}>
                            Full Job Listing</a></u></p>
                        <b>Contract Time</b>
                        <p>{time}</p>
                        <b>Contract Type</b>
                        <p>{type}</p>
                    </div>
                </div>
                <div className="map">
                    <h2 style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>{loc}</h2>
                    <iframe
                        title="map-view"
                        src={coord}
                        width="600"
                        height="450"
                        frameBorder="0"
                        style={{ border: '0' }}
                        allowFullScreen
                        aria-hidden="false" >
                    </iframe>
                <div
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                    <Button
                    variant="outlined"
                    href={'/housing/city_details/'+cityIdx}
                    >
                        <img src={housing} className="housing-photo" alt=''/>
                    </Button>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <Button
                    variant="outlined"
                    href={'/climate/city_climate/' + cityIdx}
                    >
                        <img src={climate} className="climate-photo" alt=''/>
                    </Button>
                </div>
                </div>
            </div>
        </div>

    );
}

export default JobDetails;