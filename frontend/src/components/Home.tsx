import React from 'react';
import MyCard from './HomeCards/MyCards';
import { Typography } from '@material-ui/core';

import 'bootstrap/dist/css/bootstrap.min.css';

function Home() {
    return (
        <div>
            <div className='text-center'>
                <Typography variant="h2" align="center">
                    Welcome to JobStop
                </Typography><br></br>
                <h4>We're here to help you find the best job for your needs</h4>
                <h4>Get started by exploring our data models</h4>
            </div>
            <div>
                <MyCard />
            </div>
        </div>
    );  
}
 
export default Home;