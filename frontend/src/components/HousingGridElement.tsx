import { TableRow, TableCell } from "@material-ui/core";
import React from "react";
import { Link } from "react-router-dom";


type HousingGridElementProps = {
    cityName: string,
    MLPAH: number,
    MLPFAH: number, 
    MRPAH: number, 
    MRPFAH: number, 
    PHDVAH: number,
    PHIVAH: number,
    id: number
 }
 
 export default function HousingGridElement(props: HousingGridElementProps){
    
    const city = props.cityName;
    const MLPAH = props.MLPAH;
    const MLPFAH = props.MLPFAH;
    const MRPAH = props.MRPAH;
    const MRPFAH = props.MRPFAH;
    const PHDVAH = props.PHDVAH;
    const PHIVAH = props.PHIVAH;
    const id =  props.id;
   
    /* for highlighting */
    const createMarkup = (html:string) => {
       return {__html: html}
    }

    let cells = [MLPAH.toFixed(2), MLPFAH.toFixed(2), MRPAH.toFixed(2), 
                 MRPFAH.toFixed(2), PHDVAH, PHIVAH];
 
    return(
       <TableRow>
          <TableCell dangerouslySetInnerHTML={createMarkup(city)}></TableCell>
          {
             cells.map((data:any, index:number)=>{
                return (
                <TableCell key={index}>{data}</TableCell>
                )
             })
          }
          <TableCell>
             <Link to={`/housing/city_details/${id}`}>
               <u>View Details</u></Link>
          </TableCell>
          <TableCell>
             <Link to={`/climate/city_climate/${id}`}>
               <u>Climate Details</u></Link>
          </TableCell>
       </TableRow>
    )
                
 }
 