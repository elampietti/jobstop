import React from 'react';

export type ModelGridProps = {
    records: any,
    searchFn: any,
    filterFn: any
}