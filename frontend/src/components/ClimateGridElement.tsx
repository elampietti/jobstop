import { TableRow, TableCell } from "@material-ui/core";
import React from "react";
import { Link } from "react-router-dom";

type ClimateGridElementProps = {
    cityName: string,
    lowTemp: number,
    highTemp: number,
    precipitation: number,
    typicalHumidity: number,
    typicalWindSpeed: number,
    cityId: number
}

export default function ClimateGridElement(props: ClimateGridElementProps) {

    const city = props.cityName;
    const lowestLow = props.lowTemp;
    const highestHigh = props.highTemp;
    const annualPrecipitation = props.precipitation;
    const typicalHumidity = props.typicalHumidity;
    const typicalWindSpeed = props.typicalWindSpeed;
    const id = props.cityId;

    const createMarkup = (html:string) => {
        return {__html: html}
    }

    let cells = [lowestLow, highestHigh, annualPrecipitation, typicalHumidity, 
                 typicalWindSpeed];

    return (
        <TableRow>
            <TableCell dangerouslySetInnerHTML={createMarkup(city)}></TableCell>
            {
                cells.map((data:any, index:number) => {
                    return (
                    <TableCell key={index}>{data.toFixed(2)}</TableCell>
                    )
                })
            }
            <TableCell>
                <Link to={`/climate/city_climate/${id}`}>
                    <u>View Details</u></Link>
            </TableCell>
            <TableCell>
                <Link to={`/housing/city_details/${id}`}>
                    <u>Housing Details</u></Link>
            </TableCell>
        </TableRow>
    )

}