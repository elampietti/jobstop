docker:
	docker build --pull --rm -f "Dockerfile" -t jobstop:latest "."
	docker run --rm -it -p 3000:3000/tcp jobstop:latest

build:
	npm install
	pip install flask-sqlalchemy
    pip install psycopg2-binary
    pip install sqlalchemy
    pip install flask

test-backend:
	python backend/backend_tests.py

test-frontend:
	cd frontend/
	npm run test

