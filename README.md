# JobStop

Reveal job opportunities in the U.S. for the user while taking into account first their profession, then their price range for housing, and finally their climate preferences.

## Members

|  Name | UTEID  | GitLab ID  |
|---|---|---|
|Gokul Anandaraman|gna323|Gokul_Narayan1|
|EJ Castillo|ecc2482|ej-castillo|
|Rohan Kamath|rk25782|rohan-kamath|
|Elias Lampietti|ejl2425|elampietti|
|Jonathon Lowe|jml5923|jlowe22|

## Git SHA
Phase 1: 9fb4431ab02f79526949e7a94a7af5eb98eebaf1
Phase 2: 6a5214bd43b9ebb63881faac389c9e21a8651bc4
Phase 3: 5ab33984aa238d8849005936d78010fcdb2ce5a8
Phase 4: b4b211985deda7b92edeca2ea9c628fc8198de4d

## Project Leader
Phase 1: Elias Lampietti
Phase 2: Gokul Anandaraman
Phase 3: EJ Castillo
Phase 4: Jonathon Lowe 

## Gitlab Pipelines
https://gitlab.com/elampietti/jobstop/-/pipelines

## Website Link
https://jobstop.me

## Completion Times
| Name | Estimated - Phase 1 | Actual - Phase 1 | Estimated - Phase 2| Actual - Phase 2| Estimated - Phase 3| Actual - Phase 3| Estimated - Phase 4 | Actual - Phase 4 |
|------|-----------|--------|---------|-------|---|---|---|---|
|Gokul Anandaraman|10|16|15|21|20|25|15|18|
|EJ Castillo|10|13|15|20|20|23|15|17|
|Rohan Kamath|10|15|15|20|15|18|15|15|
|Elias Lampietti|10|15|15|19|20|25|15|21|
|Jonathon Lowe|10|14|15|20|20|22|15|16|