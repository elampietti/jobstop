from flask_db import db

# Create Housing Models

class City(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    mlpah = db.Column(db.Float)
    mlpfah = db.Column(db.Float)
    mrpah = db.Column(db.Float)
    mrpfah = db.Column(db.Float)
    phdvah = db.Column(db.Float)
    phivah = db.Column(db.Float)


class MedianListingPrice(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    condo_coop = db.Column(db.Float)
    duplex_triplex = db.Column(db.Float)
    bedroom_5 = db.Column(db.Float)
    bedroom_4 = db.Column(db.Float)
    bedroom_3 = db.Column(db.Float)
    bedroom_2 = db.Column(db.Float)
    bedroom_1 = db.Column(db.Float)
    city_id = db.Column(db.Integer, db.ForeignKey("city.id"), nullable=False)
    city = db.relationship(
        "City", backref=db.backref("median_listing_price", lazy=True, 
            uselist=False)
    )


class MedianRentalPrice(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    condo_coop = db.Column(db.Float)
    duplex_triplex = db.Column(db.Float)
    bedroom_5 = db.Column(db.Float)
    bedroom_4 = db.Column(db.Float)
    bedroom_3 = db.Column(db.Float)
    bedroom_2 = db.Column(db.Float)
    bedroom_1 = db.Column(db.Float)
    city_id = db.Column(db.Integer, db.ForeignKey("city.id"), nullable=False)
    city = db.relationship(
        "City", backref=db.backref("median_rental_price", lazy=True, 
            uselist=False)
    )


class ZillowHomeValue(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    start_date = db.Column(db.DateTime)
    m1 = db.Column(db.Float)
    m2 = db.Column(db.Float)
    m3 = db.Column(db.Float)
    m4 = db.Column(db.Float)
    m5 = db.Column(db.Float)
    m6 = db.Column(db.Float)
    m7 = db.Column(db.Float)
    m8 = db.Column(db.Float)
    m9 = db.Column(db.Float)
    m10 = db.Column(db.Float)
    m11 = db.Column(db.Float)
    m12 = db.Column(db.Float)
    city_id = db.Column(db.Integer, db.ForeignKey("city.id"), nullable=False)
    city = db.relationship(
        "City", backref=db.backref("zillow_home_value", lazy=True, 
            uselist=False)
    )


class ZillowRentalValue(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    start_date = db.Column(db.DateTime)
    m1 = db.Column(db.Float)
    m2 = db.Column(db.Float)
    m3 = db.Column(db.Float)
    m4 = db.Column(db.Float)
    m5 = db.Column(db.Float)
    m6 = db.Column(db.Float)
    m7 = db.Column(db.Float)
    m8 = db.Column(db.Float)
    m9 = db.Column(db.Float)
    m10 = db.Column(db.Float)
    m11 = db.Column(db.Float)
    m12 = db.Column(db.Float)
    city_id = db.Column(db.Integer, db.ForeignKey("city.id"), nullable=False)
    city = db.relationship(
        "City", backref=db.backref("zillow_rental_value", lazy=True, 
            uselist=False)
    )
