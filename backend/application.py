from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask import jsonify, make_response
from flask_db import application, db
from housing_models import (
    City,
    MedianListingPrice,
    MedianRentalPrice,
    ZillowHomeValue,
    ZillowRentalValue,
)
from job_models import Job
from climate_models import (
    Climate,
    WindSpeedHumidity,
)
from datetime import date


""" city data related calls """


@application.route("/cities/summary", methods=["GET"])
def city_summary():
    city_idx = request.args.get("cityIdx", None)  # optional
    city_idx = city_idx if city_idx is None else int(city_idx)

    response = make_response(jsonify(construct_summary_data(city_idx)), 200)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


@application.route("/cities/cityData", methods=["GET"])
def get_city_data():

    # get the args:
    city_idx = int(request.args.get("cityIdx", 0))  # required

    data = {}
    # city rental and listing data:
    mlp_data = construct_city_instance_data(city_idx, "MLP")
    data["mlp"] = mlp_data
    mrp_data = construct_city_instance_data(city_idx, "MRP")
    data["mrp"] = mrp_data

    # zillow data
    zhviah_data = construct_city_graph_data(city_idx, "ZHVIAH")
    data["ZHVIAH"] = zhviah_data
    zrviah_data = construct_city_graph_data(city_idx, "ZRVIAH")
    data["ZRVIAH"] = zrviah_data

    # coord data
    data["coord"] = construct_lat_long(city_idx)

    # jobs located in the city
    data["jobs"] = construct_jobs_by_city(city_idx)

    response = make_response(jsonify(data), 200)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


def construct_jobs_by_city(city_idx):
    jobs = Job.query.filter_by(city_id=city_idx)
    data = {}
    for job in jobs:
        data[job.id] = {
            "company": job.company,
            "title": job.title,
            "location": job.city.name,
            "category": job.category,
            "type": job.contract_type,
        }
    return data


def construct_summary_data(city_idx):

    if city_idx is not None:
        cities = []
        cities.append(City.query.filter_by(id=city_idx).first())
    else:
        cities = City.query.all()

    data = {}
    for city in cities:
        data[city.name] = {
            "id": city.id,
            "MLPAH": city.mlpah,
            "MLPFAH": city.mlpfah,
            "MRPAH": city.mrpah,
            "MRPFAH": city.mrpfah,
            "PHDVAH": city.phdvah,
            "PHIVAH": city.phivah,
        }
    return data


def construct_city_instance_data(city_idx, indicator):
    city = City.query.filter_by(id=city_idx).first()
    if indicator == "MLP":
        raw = city.median_listing_price
    else:
        raw = city.median_rental_price
    data = {}
    # clean the data
    if raw.condo_coop != -1:
        data["Condo/Co-op"] = raw.condo_coop
    else:
        data["Condo/Co-op"] = "not available"

    if raw.duplex_triplex != -1:
        data["Duplex/Triplex"] = raw.duplex_triplex
    else:
        data["Duplex/Triplex"] = "not available"

    if raw.bedroom_5 != -1:
        data["5BR"] = raw.bedroom_5
    else:
        data["5BR"] = "not available"

    if raw.bedroom_4 != -1:
        data["4BR"] = raw.bedroom_4
    else:
        data["4BR"] = "not available"

    if raw.bedroom_3 != -1:
        data["3BR"] = raw.bedroom_3
    else:
        data["3BR"] = "not available"

    if raw.bedroom_2 != -1:
        data["2BR"] = raw.bedroom_2
    else:
        data["2BR"] = "not available"

    if raw.bedroom_1 != -1:
        data["1BR"] = raw.bedroom_1
    else:
        data["1BR"] = "not available"

    return list(data.values())


def construct_lat_long(city_idx):
    job = Job.query.filter_by(city_id=city_idx).first()
    data = {"latitude": job.latitude, "longitude": job.longitude}
    return data


def construct_city_graph_data(city_idx, indicator):
    city = City.query.filter_by(id=city_idx).first()
    if indicator == "ZHVIAH":
        raw = city.zillow_home_value
        data = [["Month", "Zillow Home Value"]]
    else:
        raw = city.zillow_rental_value
        data = [["Month", "Zillow Rental Value"]]

    date = str(raw.start_date).split()[0].split("-")
    s_m = int(date[1])
    s_y = int(date[0])
    data.extend(
        [
            [get_str_date(s_m, s_y, 0), raw.m1],
            [get_str_date(s_m, s_y, 1), raw.m2],
            [get_str_date(s_m, s_y, 2), raw.m3],
            [get_str_date(s_m, s_y, 3), raw.m4],
            [get_str_date(s_m, s_y, 4), raw.m5],
            [get_str_date(s_m, s_y, 5), raw.m6],
            [get_str_date(s_m, s_y, 6), raw.m7],
            [get_str_date(s_m, s_y, 7), raw.m8],
            [get_str_date(s_m, s_y, 8), raw.m9],
            [get_str_date(s_m, s_y, 9), raw.m10],
            [get_str_date(s_m, s_y, 10), raw.m11],
            [get_str_date(s_m, s_y, 11), raw.m12],
        ]
    )
    return data


month_list = [
    "Jan",
    "Feb",
    "March",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
]

month_dict = {
    1: "Jan",
    2: "Feb",
    3: "March",
    4: "April",
    5: "May",
    6: "June",
    7: "July",
    8: "Aug",
    9: "Sept",
    10: "Oct",
    11: "Nov",
    12: "Dec",
}


def get_str_date(start_month, start_year, time):
    year = start_year
    if start_month + time > 12:
        year += 1
        month = (start_month + time) % 12
    else:
        month = start_month + time

    return month_dict[month] + " " + str(year)


""" jobs related calls """


@application.route("/jobs", methods=["GET"])
def get_job_summary():
    # get the args:
    # possible values = summary, title, category,
    by = request.args.get("by")  # required
    val = request.args.get("value")  # optional

    if by == "summary":
        response = make_response(jsonify(construct_job_summary()), 200)

    if by == "title":
        response = make_response(jsonify(construct_jobs_title_data(val)), 200)

    if by == "category":
        response = make_response(jsonify(construct_jobs_category_data(val)), 
            200)

    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


def construct_job_summary():
    jobs = Job.query.all()
    data = {}
    for job in jobs:
        data[job.id] = {
            "company": job.company,
            "title": job.title,
            "location": job.city.name,
            "category": job.category,
            "type": job.contract_type,
        }
    return data


def construct_jobs_title_data(jtitle):
    jobs = Job.query.all()
    data = {}
    for job in jobs:
        if jtitle in job.title:
            data[job.id] = {
                "company": job.company,
                "title": job.title,
                "location": job.city.name,
                "category": job.category,
                "type": job.contract_type,
            }
    return data


def construct_jobs_category_data(jcategory):
    jobs = Job.query.all()
    data = {}
    for job in jobs:
        if jcategory in job.category:
            data[job.id] = {
                "company": job.company,
                "title": job.title,
                "location": job.city.name,
                "category": job.category,
                "type": job.contract_type,
            }
    return data


@application.route("/jobs/jobData", methods=["GET"])
def get_job_data():
    # get the args:
    job_id = request.args.get("jobIdx")
    job = Job.query.get(job_id)
    data = {}
    for i in job.__dict__:
        if i != "_sa_instance_state":
            data[i] = job.__getattribute__(i)
    data["location"] = job.city.name

    response = make_response(jsonify(data), 200)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


""" user oriented calls """


@application.route("/housing/cityPrices", methods=["GET"])
def get_housing_city_api():
    city = request.args.get("city", None)  # required
    response = make_response(jsonify(construct_housing_city_data(city)))
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


def construct_housing_city_data(thiscity):
    city = City.query.filter_by(name=thiscity).first()
    mlp = city.median_listing_price
    mrp = city.median_rental_price
    data = {}
    data["housing"] = {
        "city": city.name,
        "1-bedroom": mlp.bedroom_1,
        "2-bedroom": mlp.bedroom_2,
        "3-bedroom": mlp.bedroom_3,
        "4-bedroom": mlp.bedroom_4,
        "5-bedroom": mlp.bedroom_5,
    }
    data["rental"] = {
        "city": city.name,
        "1-bedroom": mrp.bedroom_1,
        "2-bedroom": mrp.bedroom_2,
        "3-bedroom": mrp.bedroom_3,
        "4-bedroom": mrp.bedroom_4,
        "5-bedroom": mrp.bedroom_5,
    }
    return data


@application.route("/housing/jobStop", methods=["GET"])
def get_jobstop_cities_api():
    # all args are optional, if no args are given, we simply return city
    # summary data.
    title = request.args.get("jobTitle", None)

    low = request.args.get("climatelow", None)
    low = low if low is None else int(low)

    high = request.args.get("climatehigh", None)
    high = high if high is None else int(high)

    rain = request.args.get("climaterain", None)
    rain = rain if rain is None else int(rain)

    hprice = request.args.get("housingprice", None)
    hprice = hprice if hprice is None else int(hprice)

    rprice = request.args.get("rentalprice", None)
    rprice = rprice if rprice is None else int(rprice)

    response = make_response(
        jsonify(construct_jobstop_cities_data(title, low, high, rain, hprice, 
            rprice))
    )
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


def construct_jobstop_cities_data(title, low, high, rain, hprice, rprice):
    jobs = Job.query.all()
    data = {}
    cities = []
    if title is not None:
        for job in jobs:
            if title in job.title:
                cities.append(job.city)

    tmp = []
    for c in cities:
        add = True
        if hprice is not None and c.mlpah > hprice:
            add = False
        if rprice is not None and c.mlpah > rprice:
            add = False
        if low is not None and c.climate.m6_tmin > low + 10:
            add = False
        if low is not None and c.climate.m6_tmin < low - 10:
            add = False
        if high is not None and c.climate.m6_tmax > high + 10:
            add = False
        if high is not None and c.climate.m6_tmax < high - 10:
            add = False
        if rain is not None and abs(c.climate.m6_prcp - rain) > 10:
            add = False

        if add:
            data[c.name] = {
                "id": c.id,
                "MLPAH": c.mlpah,
                "MLPFAH": c.mlpfah,
                "MRPAH": c.mrpah,
                "MRPFAH": c.mrpfah,
                "PHDVAH": c.phdvah,
                "PHIVAH": c.phivah,
            }
    return data


""" climate related calls """


@application.route("/climate")
def get_climate_summary():
    # get args:
    by = request.args.get("by")  # required, can be summary or city_idx
    city_id = request.args.get("value")  # optional

    if by == "summary":
        data = construct_climate_summary()
    elif by == "cityIdx":
        temp = construct_climate_data(city_id)
        # get the city name:
        name = City.query.filter_by(id=city_id).first().name
        data = {name: temp}

    response = make_response(jsonify(data), 200)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


def construct_climate_summary():
    climates = Climate.query.all()
    windSpeedsHumidities = WindSpeedHumidity.query.all()
    data = {}
    for row in climates:
        city_id = row.city_id
        data[row.city.name] = construct_climate_data(city_id)
    return data


def construct_climate_data(city_id):
    climates = Climate.query.filter_by(city_id=city_id).first()
    data = {}

    lowTemps = [
        climates.m1_tmin,
        climates.m2_tmin,
        climates.m3_tmin,
        climates.m4_tmin,
        climates.m5_tmin,
        climates.m6_tmin,
        climates.m7_tmin,
        climates.m8_tmin,
        climates.m9_tmin,
        climates.m10_tmin,
        climates.m11_tmin,
        climates.m12_tmin,
    ]
    highTemps = [
        climates.m1_tmax,
        climates.m2_tmax,
        climates.m3_tmax,
        climates.m4_tmax,
        climates.m5_tmax,
        climates.m6_tmax,
        climates.m7_tmax,
        climates.m8_tmax,
        climates.m9_tmax,
        climates.m10_tmax,
        climates.m11_tmax,
        climates.m12_tmax,
    ]
    precipitation = [
        climates.m1_prcp,
        climates.m2_prcp,
        climates.m3_prcp,
        climates.m4_prcp,
        climates.m5_prcp,
        climates.m6_prcp,
        climates.m7_prcp,
        climates.m8_prcp,
        climates.m9_prcp,
        climates.m10_prcp,
        climates.m11_prcp,
        climates.m12_prcp,
    ]
    combinedTemps = [["Month", "Low", "High"]]

    formattedPrecipitation = [["Month", "Precipitation (mm)"]]
    for i in range(12):
        formattedPrecipitation.append([month_list[i], precipitation[i]])
        combinedTemps.append([month_list[i], lowTemps[i], highTemps[i]])

    historical_data = {
        "cityId": climates.city_id,
        "lowTemps": lowTemps,
        "highTemps": highTemps,
        "combinedTemps": combinedTemps,
        "precipitation": precipitation,
        "formattedPrecipitation": formattedPrecipitation,
    }
    data = historical_data

    append_wind_humidity(city_id, data)

    summary = analyze_forecast(data)
    data["summary"] = summary
    return data


def append_wind_humidity(city_id, data):
    wind_humidity = WindSpeedHumidity.query.filter_by(city_id=city_id).first()

    humidities = [
        wind_humidity.m1_humidity,
        wind_humidity.m2_humidity,
        wind_humidity.m3_humidity,
        wind_humidity.m4_humidity,
        wind_humidity.m5_humidity,
        wind_humidity.m6_humidity,
        wind_humidity.m7_humidity,
        wind_humidity.m8_humidity,
        wind_humidity.m9_humidity,
        wind_humidity.m10_humidity,
        wind_humidity.m11_humidity,
        wind_humidity.m12_humidity,
    ]
    windSpeeds = [
        wind_humidity.m1_wind_speed,
        wind_humidity.m2_wind_speed,
        wind_humidity.m3_wind_speed,
        wind_humidity.m4_wind_speed,
        wind_humidity.m5_wind_speed,
        wind_humidity.m6_wind_speed,
        wind_humidity.m7_wind_speed,
        wind_humidity.m8_wind_speed,
        wind_humidity.m9_wind_speed,
        wind_humidity.m10_wind_speed,
        wind_humidity.m11_wind_speed,
        wind_humidity.m12_wind_speed,
    ]
    data["humidities"] = humidities
    data["windSpeeds"] = windSpeeds


def analyze_forecast(data):
    thisMonth = int(date.today().strftime("%m")) - 1
    highTemp = data["highTemps"][thisMonth]
    precipitation = data["precipitation"][thisMonth]
    windSpeed = data["windSpeeds"][thisMonth]
    if precipitation / 30 > 2.5:
        if highTemp < 4:
            return "snow"
        return "rain"
    else:
        if windSpeed > 14:
            return "windy"
        else:
            return "sunny"


if __name__ == "__main__":
    application.run(host="0.0.0.0")
