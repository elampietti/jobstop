import unittest
import application


class BackendTests(unittest.TestCase):

    """ construct_summary_data """

    def test_1(self):
        data = application.construct_summary_data(None)
        self.assertEqual(len(data), 100)

    def test_2(self):
        data = application.construct_summary_data(None)
        assert "Albuquerque, New Mexico" in data.keys()
        assert data["Albuquerque, New Mexico"] == {
            "MLPAH": 235000.0,
            "MLPFAH": 133.917922774215,
            "MRPAH": 1300.0,
            "MRPFAH": 0.897226753670473,
            "PHDVAH": 29.92,
            "PHIVAH": 68.95,
            "id": 501,
        }

    def test_3(self):
        data = application.construct_summary_data(502)
        assert type(data) == dict
        assert data["Anaheim, California"] == {
            "MLPAH": 610000.0,
            "MLPFAH": 389.194630872483,
            "MRPAH": 2700.0,
            "MRPFAH": 1.83055849024224,
            "PHDVAH": 95.19,
            "PHIVAH": 4.55,
            "id": 502,
        }

    """ construct_city_instance_data(city_idx, indicator) """

    def test_4(self):
        data = application.construct_city_instance_data(501, "MLP")
        assert type(data) == list
        assert data == [
            155000.0,
            "not available",
            442781.0,
            315000.0,
            225000.0,
            165000.0,
            155900.0,
        ]

    def test_5(self):
        data = application.construct_city_instance_data(501, "MRP")
        assert len(data) == 7
        assert data == [1087.0, 695.0, "not available", 1450.0, 1350.0, 991.0, 
            771.5]

    def test_6(self):
        data1 = application.construct_city_instance_data(503, "MLP")
        data2 = application.construct_city_instance_data(503, "MRP")
        assert len(data1) == len(data2)
        assert data1 != data2

    """ construct_lat_long """

    def test_7(self):
        data = application.construct_lat_long(501)
        assert type(data) == dict
        assert data["latitude"] == 35.0846
        assert data["longitude"] == -106.6516

    def test_8(self):
        data = application.construct_lat_long(502)
        assert type(data) == dict
        assert data == {"latitude": 33.846981, "longitude": -117.954189}

    """ construct_city_graph_data """

    def test_9(self):
        data = application.construct_city_graph_data(501, "ZHVIAH")
        assert type(data) == list
        assert data == [
            ["Month", "Zillow Home Value"],
            ["April 2019", 202469.0],
            ["May 2019", 203085.0],
            ["June 2019", 204150.0],
            ["July 2019", 205725.0],
            ["Aug 2019", 207146.0],
            ["Sept 2019", 208548.0],
            ["Oct 2019", 209262.0],
            ["Nov 2019", 210364.0],
            ["Dec 2019", 211548.0],
            ["Jan 2020", 212921.0],
            ["Feb 2020", 214485.0],
            ["March 2020", 216090.0],
        ]

    def test_10(self):
        data = application.construct_city_graph_data(501, "ZRVIAH")
        assert type(data) == list
        assert data == [
            ["Month", "Zillow Rental Value"],
            ["March 2019", 1255.0],
            ["April 2019", 1258.0],
            ["May 2019", 1263.0],
            ["June 2019", 1270.0],
            ["July 2019", 1277.0],
            ["Aug 2019", 1284.0],
            ["Sept 2019", 1289.0],
            ["Oct 2019", 1308.0],
            ["Nov 2019", 1316.0],
            ["Dec 2019", 1321.0],
            ["Jan 2020", 1324.0],
            ["Feb 2020", 1301.0],
        ]

    def test_11(self):
        data1 = application.construct_city_graph_data(502, "ZRVIAH")
        data2 = application.construct_city_graph_data(502, "ZHVIAH")
        assert data1 != data2

    """ get_str_date """

    def test_12(self):
        date = application.get_str_date(3, 2020, 11)
        assert date == "Feb 2021"

    def test_13(self):
        date = application.get_str_date(5, 2020, 3)
        assert date == "Aug 2020"

    """ construct jobs by city """

    def test_14(self):
        data = application.construct_jobs_by_city(501)
        assert len(data) == 10
        actual = {
            "category": "IT Jobs",
            "company": "Intuit",
            "location": "Albuquerque, New Mexico",
            "title": "Credentialed Tax Professional - CPA, Enrolled Agent or Attorney - Remote",
            "type": "Not specified",
        }
        assert data[1] == actual

    def test_15(self):
        data = application.construct_jobs_by_city(503)
        assert len(data) == 10
        actual = {
            "category": "Healthcare & Nursing Jobs",
            "company": "Jackson Therapy Partners",
            "location": "Anchorage, Alaska",
            "title": "Speech Language Pathologist - Travel Contract - New Grads Welcome",
            "type": "Not specified",
        }
        assert data[27] == actual

    """ construct_job_summary """

    def test_16(self):
        data = application.construct_job_summary()
        self.assertEqual(len(data), 1000)

    def test_17(self):
        data = application.construct_job_summary()
        self.assertEqual(type(data[1]), dict)

    """ construct_jobs_title_data """

    def test_18(self):
        data = application.construct_jobs_title_data("Barista")
        self.assertEqual(len(data), 1)

    def test_19(self):
        data = application.construct_jobs_title_data("Barista")
        self.assertEqual(type(data), dict)

    def test_20(self):
        data = application.construct_jobs_title_data("Barista")
        self.assertEqual(data[144]["company"], "Snooze")

    """ construct_jobs_category_data """

    def test_21(self):
        data = application.construct_jobs_category_data("IT Jobs")
        self.assertEqual(len(data), 71)

    def test_22(self):
        data = application.construct_jobs_category_data("IT Jobs")
        self.assertEqual(type(data), dict)

    def test_23(self):
        data = application.construct_jobs_category_data("IT Jobs")
        self.assertEqual(data[1]["company"], "Intuit")

    """ construct_climate_summary """

    def test_24(self):
        data = application.construct_climate_summary()
        self.assertEqual(len(data), 100)

    def test_25(self):
        data = application.construct_climate_summary()
        self.assertEqual(type(data), dict)

    def test_26(self):
        data = application.construct_climate_summary()
        self.assertEqual(next(iter(data)), "Albuquerque, New Mexico")

    """ construct_climate_data """

    def test_27(self):
        data = application.construct_climate_data(501)
        self.assertEqual(len(data), 9)

    def test_28(self):
        data = application.construct_climate_data(501)
        self.assertEqual(type(data), dict)

    def test_29(self):
        data = application.construct_climate_data(501)
        self.assertEqual(next(iter(data)), "cityId")

    """ construct_housing_city_data """

    def test_36(self):
        data = application.construct_housing_city_data("Buffalo, New York")
        self.assertEqual(len(data), 2)

    def test_37(self):
        data = application.construct_housing_city_data("Buffalo, New York")
        self.assertEqual(type(data), dict)

    def test_38(self):
        data = application.construct_housing_city_data("Buffalo, New York")
        self.assertEqual(next(iter(data)), "housing")

    """ construct_jobstop_cities_data """

    def test_39(self):
        data = application.construct_jobstop_cities_data(
            "Tutor", 15, 26, 10, 900000, 500000
        )
        self.assertEqual(len(data), 3)

    def test_40(self):
        data = application.construct_jobstop_cities_data(
            "Tutor", 15, 26, 10, 900000, 500000
        )
        self.assertEqual(type(data), dict)

    def test_41(self):
        data = application.construct_jobstop_cities_data(
            "Tutor", 15, 26, 10, 900000, 500000
        )
        self.assertEqual(next(iter(data)), "El Paso, Texas")

    """ test append wind humidity """

    def test_43(self):
        data = {}
        application.append_wind_humidity(501, data)
        assert data["humidities"]
        assert data["windSpeeds"]

    """ test analyze forecast """

    def test_44(self):
        coldTemps = [0] * 12
        hotTemps = [40] * 12
        windyList = [20] * 12
        notWindyList = [0] * 12
        rainyList = [90] * 12
        droughtList = [0] * 12
        sunnyOne = application.analyze_forecast(
            {
                "highTemps": hotTemps,
                "precipitation": droughtList,
                "windSpeeds": notWindyList,
            }
        )
        sunnyTwo = application.analyze_forecast(
            {
                "highTemps": coldTemps,
                "precipitation": droughtList,
                "windSpeeds": notWindyList,
            }
        )
        rainy = application.analyze_forecast(
            {
                "highTemps": hotTemps,
                "precipitation": rainyList,
                "windSpeeds": notWindyList,
            }
        )
        snowy = application.analyze_forecast(
            {
                "highTemps": coldTemps,
                "precipitation": rainyList,
                "windSpeeds": notWindyList,
            }
        )
        windy = application.analyze_forecast(
            {
                "highTemps": hotTemps,
                "precipitation": droughtList,
                "windSpeeds": windyList,
            }
        )
        rainyWindy = application.analyze_forecast(
            {"highTemps": hotTemps, "precipitation": rainyList, 
                "windSpeeds": windyList}
        )
        expected = ["sunny", "sunny", "rain", "snow", "windy", "rain"]
        actual = [sunnyOne, sunnyTwo, rainy, snowy, windy, rainyWindy]
        assert expected == actual


if __name__ == "__main__":
    unittest.main()
