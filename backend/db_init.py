from flask_db import db
from housing_models.py import (
    City,
    MedianListingPrice,
    MedianRentalPrice,
    ZillowHomeValue,
    ZillowRentalValue,
)
from job_models.py import Job
from climate_models.py import (
    Climate,
    WindSpeedHumidity,
)
import pandas as pd
import numpy as np
from psycopg2.extensions import register_adapter, AsIs

# Handle numpy inputs
def addapt_numpy_float64(numpy_float64):
    return AsIs(numpy_float64)

def addapt_numpy_int64(numpy_int64):
    return AsIs(numpy_int64)


register_adapter(np.float64, addapt_numpy_float64)
register_adapter(np.int64, addapt_numpy_int64)
# Creates all database tables
# db.create_all()

CREATE_HOUSING = False
CREATE_JOBS = False
CREATE_CLIMATE = False

if CREATE_HOUSING:
    # Put data into pandas DataFrames
    summary_df = pd.read_csv("data/summary.csv", index_col=0)
    mlp_df = pd.read_csv("data/MLP.csv", index_col=0)
    mrp_df = pd.read_csv("data/MRP.csv", index_col=0)
    zhv_df = pd.read_csv("data/ZHV.csv", index_col=0)
    zrv_df = pd.read_csv("data/ZRV.csv", index_col=0)
    for i in summary_df.index:
        # Create city entry
        city = City(
            name=summary_df["city"][i],
            mlpah=summary_df["MLPAH"][i],
            mlpfah=summary_df["MLPFAH"][i],
            mrpah=summary_df["MRPAH"][i],
            mrpfah=summary_df["MRPFAH"][i],
            phdvah=summary_df["PHDVAH"][i],
            phivah=summary_df["PHIVAH"][i],
        )
        # Create corresponding MedianListingPrice entry for this city
        mlp_row = (
            mlp_df.loc[mlp_df["city"] == summary_df["city"][i]]
            .reset_index(drop=True)
            .iloc[0]
        )
        mlp = MedianListingPrice(
            condo_coop=mlp_row["condo/co-op"],
            duplex_triplex=mlp_row["duplex/triplex"],
            bedroom_5=mlp_row["5 bedroom +"],
            bedroom_4=mlp_row["4 bedroom"],
            bedroom_3=mlp_row["3 bedroom"],
            bedroom_2=mlp_row["2 bedroom"],
            bedroom_1=mlp_row["1 bedroom"],
            city=city,
        )
        # Create corresponding MedianRentalPrice entry for this city
        mrp_row = (
            mrp_df.loc[mrp_df["city"] == summary_df["city"][i]]
            .reset_index(drop=True)
            .iloc[0]
        )
        mrp = MedianRentalPrice(
            condo_coop=mrp_row["condo/co-op"],
            duplex_triplex=mrp_row["duplex/triplex"],
            bedroom_5=mrp_row["5 bedroom +"],
            bedroom_4=mrp_row["4 bedroom"],
            bedroom_3=mrp_row["3 bedroom"],
            bedroom_2=mrp_row["2 bedroom"],
            bedroom_1=mrp_row["1 bedroom"],
            city=city,
        )
        # Create corresponding ZillowHomeValue entry for this city
        zhv_row = (
            zhv_df.loc[zhv_df["city"] == summary_df["city"][i]]
            .reset_index(drop=True)
            .iloc[0]
        )
        zhv = ZillowHomeValue(
            start_date=zhv_row["starting date"],
            m1=zhv_row["m1"],
            m2=zhv_row["m2"],
            m3=zhv_row["m3"],
            m4=zhv_row["m4"],
            m5=zhv_row["m5"],
            m6=zhv_row["m6"],
            m7=zhv_row["m7"],
            m8=zhv_row["m8"],
            m9=zhv_row["m9"],
            m10=zhv_row["m10"],
            m11=zhv_row["m11"],
            m12=zhv_row["m12"],
            city=city,
        )
        # Create corresponding ZillowRentalValue entry for this city
        zrv_row = (
            zrv_df.loc[zrv_df["city"] == summary_df["city"][i]]
            .reset_index(drop=True)
            .iloc[0]
        )
        zrv = ZillowRentalValue(
            start_date=zrv_row["starting date"],
            m1=zrv_row["m1"],
            m2=zrv_row["m2"],
            m3=zrv_row["m3"],
            m4=zrv_row["m4"],
            m5=zrv_row["m5"],
            m6=zrv_row["m6"],
            m7=zrv_row["m7"],
            m8=zrv_row["m8"],
            m9=zrv_row["m9"],
            m10=zrv_row["m10"],
            m11=zrv_row["m11"],
            m12=zrv_row["m12"],
            city=city,
        )
        # Add created table entries to database session
        db.session.add(city)
        db.session.add(mlp)
        db.session.add(mrp)
        db.session.add(zhv)
        db.session.add(zrv)

    # Populate database with entries from current session
    # db.session.commit()

if CREATE_JOBS:
    # Put data into pandas DataFrames
    job_df = pd.read_csv("data/jobs.csv")
    count = 0
    for i in job_df.index:
        city = None
        cities = City.query.all()
        for potential_city in cities:
            if potential_city.name == job_df["city"][i]:
                city = potential_city
                break
        if city:
            count += 1
            print(count)
            # Create corresponding Job entry for this city
            job_row = job_df.iloc[i]
            job = Job(
                job_id=job_row["job_id"],
                created=job_row["created"],
                company=job_row["company"],
                title=job_row["title"],
                category=job_row["category"],
                description=job_row["description"],
                latitude=job_row["latitude"],
                longitude=job_row["longitude"],
                contract_type=job_row["contract_type"],
                contract_time=job_row["contract_time"],
                redirect_url=job_row["redirect_url"],
                domain=job_row["domain"],
                city=city,
            )
            # Add created table entry to database session
            db.session.add(job)

    # Populate database with entries from current session
    # db.session.commit()

if CREATE_CLIMATE:
    # Put data into pandas DataFrames
    climate_df = pd.read_csv("data/climate.csv")
    count = 0
    for i in climate_df.index:
        city = None
        cities = City.query.all()
        for potential_city in cities:
            if potential_city.name == climate_df["City"][i]:
                city = potential_city
                break
        if city:
            count += 1
            print(count)
            # Create corresponding Climate entry for this city
            climate_row = climate_df.iloc[i]
            climate = Climate(
                m1_tmin=climate_row["month1 tmin"],
                m2_tmin=climate_row["month2 tmin"],
                m3_tmin=climate_row["month3 tmin"],
                m4_tmin=climate_row["month4 tmin"],
                m5_tmin=climate_row["month5 tmin"],
                m6_tmin=climate_row["month6 tmin"],
                m7_tmin=climate_row["month7 tmin"],
                m8_tmin=climate_row["month8 tmin"],
                m9_tmin=climate_row["month9 tmin"],
                m10_tmin=climate_row["month10 tmin"],
                m11_tmin=climate_row["month11 tmin"],
                m12_tmin=climate_row["month12 tmin"],
                m1_tmax=climate_row["month1 tmax"],
                m2_tmax=climate_row["month2 tmax"],
                m3_tmax=climate_row["month3 tmax"],
                m4_tmax=climate_row["month4 tmax"],
                m5_tmax=climate_row["month5 tmax"],
                m6_tmax=climate_row["month6 tmax"],
                m7_tmax=climate_row["month7 tmax"],
                m8_tmax=climate_row["month8 tmax"],
                m9_tmax=climate_row["month9 tmax"],
                m10_tmax=climate_row["month10 tmax"],
                m11_tmax=climate_row["month11 tmax"],
                m12_tmax=climate_row["month12 tmax"],
                m1_prcp=climate_row["month1 prcp"],
                m2_prcp=climate_row["month2 prcp"],
                m3_prcp=climate_row["month3 prcp"],
                m4_prcp=climate_row["month4 prcp"],
                m5_prcp=climate_row["month5 prcp"],
                m6_prcp=climate_row["month6 prcp"],
                m7_prcp=climate_row["month7 prcp"],
                m8_prcp=climate_row["month8 prcp"],
                m9_prcp=climate_row["month9 prcp"],
                m10_prcp=climate_row["month10 prcp"],
                m11_prcp=climate_row["month11 prcp"],
                m12_prcp=climate_row["month12 prcp"],
                city=city,
            )
            # Add created table entry to database session
            db.session.add(climate)

    windspeed_humidity_df = pd.read_csv("data/wind_speed_humidity.csv")
    count = 0
    for i in windspeed_humidity_df.index:
        city = None
        cities = City.query.all()
        for potential_city in cities:
            if potential_city.name == windspeed_humidity_df["city"][i]:
                city = potential_city
                break
        if city:
            count += 1
            print(count)
            # Create corresponding WindSpeedHumidity entry for this city
            windspeed_humidity_row = windspeed_humidity_df.iloc[i]
            windspeed_humidity = WindSpeedHumidity(
                m1_humidity=windspeed_humidity_row["m1_humidity"],
                m2_humidity=windspeed_humidity_row["m2_humidity"],
                m3_humidity=windspeed_humidity_row["m3_humidity"],
                m4_humidity=windspeed_humidity_row["m4_humidity"],
                m5_humidity=windspeed_humidity_row["m5_humidity"],
                m6_humidity=windspeed_humidity_row["m6_humidity"],
                m7_humidity=windspeed_humidity_row["m7_humidity"],
                m8_humidity=windspeed_humidity_row["m8_humidity"],
                m9_humidity=windspeed_humidity_row["m9_humidity"],
                m10_humidity=windspeed_humidity_row["m10_humidity"],
                m11_humidity=windspeed_humidity_row["m11_humidity"],
                m12_humidity=windspeed_humidity_row["m12_humidity"],
                m1_wind_speed=windspeed_humidity_row["m1_windSpeed"],
                m2_wind_speed=windspeed_humidity_row["m2_windSpeed"],
                m3_wind_speed=windspeed_humidity_row["m3_windSpeed"],
                m4_wind_speed=windspeed_humidity_row["m4_windSpeed"],
                m5_wind_speed=windspeed_humidity_row["m5_windSpeed"],
                m6_wind_speed=windspeed_humidity_row["m6_windSpeed"],
                m7_wind_speed=windspeed_humidity_row["m7_windSpeed"],
                m8_wind_speed=windspeed_humidity_row["m8_windSpeed"],
                m9_wind_speed=windspeed_humidity_row["m9_windSpeed"],
                m10_wind_speed=windspeed_humidity_row["m10_windSpeed"],
                m11_wind_speed=windspeed_humidity_row["m11_windSpeed"],
                m12_wind_speed=windspeed_humidity_row["m12_windSpeed"],
                city=city,
            )
            # Add created table entry to database session
            db.session.add(windspeed_humidity)
