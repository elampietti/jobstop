from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

# Create the Flask application and the Flask-SQLAlchemy object.
application= Flask(__name__)
application.config["DEBUG"]= True
application.config["SQLALCHEMY_TRACK_MODIFICATIONS"]= False
application.config["SQLALCHEMY_DATABASE_URI"]= (os.environ["K8S_SECRET_DB_URI"])
db= SQLAlchemy(application) 