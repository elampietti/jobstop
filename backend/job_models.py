from flask_db import db

# Create Job Model
class Job(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    job_id = db.Column(db.Integer)
    created = db.Column(db.DateTime)
    company = db.Column(db.String(1000))
    title = db.Column(db.String(1000))
    category = db.Column(db.String(1000))
    description = db.Column(db.String(1000))
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    contract_type = db.Column(db.String(1000))
    contract_time = db.Column(db.String(1000))
    redirect_url = db.Column(db.String(1000))
    domain = db.Column(db.String(1000))
    city_id = db.Column(db.Integer, db.ForeignKey("city.id"), nullable=False)
    city = db.relationship("City", backref=db.backref("Job", lazy=True, 
        uselist=True))